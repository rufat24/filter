/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { DocumentsDetailComponent } from '../../../../../../main/webapp/app/entities/documents/documents-detail.component';
import { DocumentsService } from '../../../../../../main/webapp/app/entities/documents/documents.service';
import { Documents } from '../../../../../../main/webapp/app/entities/documents/documents.model';

describe('Component Tests', () => {

    describe('Documents Management Detail Component', () => {
        let comp: DocumentsDetailComponent;
        let fixture: ComponentFixture<DocumentsDetailComponent>;
        let service: DocumentsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [DocumentsDetailComponent],
                providers: [
                    DocumentsService
                ]
            })
            .overrideTemplate(DocumentsDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DocumentsDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DocumentsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Documents(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.documents).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
