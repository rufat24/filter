/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { DocumentsComponent } from '../../../../../../main/webapp/app/entities/documents/documents.component';
import { DocumentsService } from '../../../../../../main/webapp/app/entities/documents/documents.service';
import { Documents } from '../../../../../../main/webapp/app/entities/documents/documents.model';

describe('Component Tests', () => {

    describe('Documents Management Component', () => {
        let comp: DocumentsComponent;
        let fixture: ComponentFixture<DocumentsComponent>;
        let service: DocumentsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [DocumentsComponent],
                providers: [
                    DocumentsService
                ]
            })
            .overrideTemplate(DocumentsComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DocumentsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DocumentsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Documents(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.documents[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
