/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { BusComponent } from '../../../../../../main/webapp/app/entities/bus/bus.component';
import { BusService } from '../../../../../../main/webapp/app/entities/bus/bus.service';
import { Bus } from '../../../../../../main/webapp/app/entities/bus/bus.model';

describe('Component Tests', () => {

    describe('Bus Management Component', () => {
        let comp: BusComponent;
        let fixture: ComponentFixture<BusComponent>;
        let service: BusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [BusComponent],
                providers: [
                    BusService
                ]
            })
            .overrideTemplate(BusComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BusComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Bus(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.buses[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
