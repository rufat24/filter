/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { BusDetailComponent } from '../../../../../../main/webapp/app/entities/bus/bus-detail.component';
import { BusService } from '../../../../../../main/webapp/app/entities/bus/bus.service';
import { Bus } from '../../../../../../main/webapp/app/entities/bus/bus.model';

describe('Component Tests', () => {

    describe('Bus Management Detail Component', () => {
        let comp: BusDetailComponent;
        let fixture: ComponentFixture<BusDetailComponent>;
        let service: BusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [BusDetailComponent],
                providers: [
                    BusService
                ]
            })
            .overrideTemplate(BusDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BusDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Bus(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.bus).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
