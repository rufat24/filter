/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { SystemSettingsDetailComponent } from '../../../../../../main/webapp/app/entities/system-settings/system-settings-detail.component';
import { SystemSettingsService } from '../../../../../../main/webapp/app/entities/system-settings/system-settings.service';
import { SystemSettings } from '../../../../../../main/webapp/app/entities/system-settings/system-settings.model';

describe('Component Tests', () => {

    describe('SystemSettings Management Detail Component', () => {
        let comp: SystemSettingsDetailComponent;
        let fixture: ComponentFixture<SystemSettingsDetailComponent>;
        let service: SystemSettingsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [SystemSettingsDetailComponent],
                providers: [
                    SystemSettingsService
                ]
            })
            .overrideTemplate(SystemSettingsDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SystemSettingsDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SystemSettingsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SystemSettings(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.systemSettings).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
