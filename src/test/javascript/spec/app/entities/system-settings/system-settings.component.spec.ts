/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { SystemSettingsComponent } from '../../../../../../main/webapp/app/entities/system-settings/system-settings.component';
import { SystemSettingsService } from '../../../../../../main/webapp/app/entities/system-settings/system-settings.service';
import { SystemSettings } from '../../../../../../main/webapp/app/entities/system-settings/system-settings.model';

describe('Component Tests', () => {

    describe('SystemSettings Management Component', () => {
        let comp: SystemSettingsComponent;
        let fixture: ComponentFixture<SystemSettingsComponent>;
        let service: SystemSettingsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [SystemSettingsComponent],
                providers: [
                    SystemSettingsService
                ]
            })
            .overrideTemplate(SystemSettingsComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SystemSettingsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SystemSettingsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SystemSettings(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.systemSettings[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
