package com.blabla.tripmanager.web.rest;

import com.blabla.tripmanager.TestApp;

import com.blabla.tripmanager.domain.Tour;
import com.blabla.tripmanager.repository.TourRepository;
import com.blabla.tripmanager.service.TourService;
import com.blabla.tripmanager.service.dto.TourDTO;
import com.blabla.tripmanager.service.mapper.TourMapper;
import com.blabla.tripmanager.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.blabla.tripmanager.web.rest.TestUtil.sameInstant;
import static com.blabla.tripmanager.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TourResource REST controller.
 *
 * @see TourResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class TourResourceIntTest {

    private static final Integer DEFAULT_GROUP_NUMBER = 1;
    private static final Integer UPDATED_GROUP_NUMBER = 2;

    private static final String DEFAULT_START_PLACE = "AAAAAAAAAA";
    private static final String UPDATED_START_PLACE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_START_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_END_PLACE = "AAAAAAAAAA";
    private static final String UPDATED_END_PLACE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_END_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_PASSENGER_AMOUNT = 1;
    private static final Integer UPDATED_PASSENGER_AMOUNT = 2;

    private static final Boolean DEFAULT_IS_INVOICED = false;
    private static final Boolean UPDATED_IS_INVOICED = true;

    private static final Integer DEFAULT_PAYMENT_AMOUNT = 1;
    private static final Integer UPDATED_PAYMENT_AMOUNT = 2;

    private static final Boolean DEFAULT_IS_INVOICE_PAID = false;
    private static final Boolean UPDATED_IS_INVOICE_PAID = true;

    private static final ZonedDateTime DEFAULT_PAYMENT_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PAYMENT_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private TourRepository tourRepository;

    @Autowired
    private TourMapper tourMapper;

    @Autowired
    private TourService tourService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTourMockMvc;

    private Tour tour;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TourResource tourResource = new TourResource(tourService);
        this.restTourMockMvc = MockMvcBuilders.standaloneSetup(tourResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tour createEntity(EntityManager em) {
        Tour tour = new Tour()
            .groupNumber(DEFAULT_GROUP_NUMBER)
            .startPlace(DEFAULT_START_PLACE)
            .startDateTime(DEFAULT_START_DATE_TIME)
            .endPlace(DEFAULT_END_PLACE)
            .endDateTime(DEFAULT_END_DATE_TIME)
            .passengerAmount(DEFAULT_PASSENGER_AMOUNT)
            .isInvoiced(DEFAULT_IS_INVOICED)
            .paymentAmount(DEFAULT_PAYMENT_AMOUNT)
            .isInvoicePaid(DEFAULT_IS_INVOICE_PAID)
            .paymentDate(DEFAULT_PAYMENT_DATE);
        return tour;
    }

    @Before
    public void initTest() {
        tour = createEntity(em);
    }

    @Test
    @Transactional
    public void createTour() throws Exception {
        int databaseSizeBeforeCreate = tourRepository.findAll().size();

        // Create the Tour
        TourDTO tourDTO = tourMapper.toDto(tour);
        restTourMockMvc.perform(post("/api/tours")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tourDTO)))
            .andExpect(status().isCreated());

        // Validate the Tour in the database
        List<Tour> tourList = tourRepository.findAll();
        assertThat(tourList).hasSize(databaseSizeBeforeCreate + 1);
        Tour testTour = tourList.get(tourList.size() - 1);
        assertThat(testTour.getGroupNumber()).isEqualTo(DEFAULT_GROUP_NUMBER);
        assertThat(testTour.getStartPlace()).isEqualTo(DEFAULT_START_PLACE);
        assertThat(testTour.getStartDateTime()).isEqualTo(DEFAULT_START_DATE_TIME);
        assertThat(testTour.getEndPlace()).isEqualTo(DEFAULT_END_PLACE);
        assertThat(testTour.getEndDateTime()).isEqualTo(DEFAULT_END_DATE_TIME);
        assertThat(testTour.getPassengerAmount()).isEqualTo(DEFAULT_PASSENGER_AMOUNT);
        assertThat(testTour.isIsInvoiced()).isEqualTo(DEFAULT_IS_INVOICED);
        assertThat(testTour.getPaymentAmount()).isEqualTo(DEFAULT_PAYMENT_AMOUNT);
        assertThat(testTour.isIsInvoicePaid()).isEqualTo(DEFAULT_IS_INVOICE_PAID);
        assertThat(testTour.getPaymentDate()).isEqualTo(DEFAULT_PAYMENT_DATE);
    }

    @Test
    @Transactional
    public void createTourWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tourRepository.findAll().size();

        // Create the Tour with an existing ID
        tour.setId(1L);
        TourDTO tourDTO = tourMapper.toDto(tour);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTourMockMvc.perform(post("/api/tours")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tourDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tour in the database
        List<Tour> tourList = tourRepository.findAll();
        assertThat(tourList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTours() throws Exception {
        // Initialize the database
        tourRepository.saveAndFlush(tour);

        // Get all the tourList
        restTourMockMvc.perform(get("/api/tours?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tour.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupNumber").value(hasItem(DEFAULT_GROUP_NUMBER)))
            .andExpect(jsonPath("$.[*].startPlace").value(hasItem(DEFAULT_START_PLACE.toString())))
            .andExpect(jsonPath("$.[*].startDateTime").value(hasItem(sameInstant(DEFAULT_START_DATE_TIME))))
            .andExpect(jsonPath("$.[*].endPlace").value(hasItem(DEFAULT_END_PLACE.toString())))
            .andExpect(jsonPath("$.[*].endDateTime").value(hasItem(sameInstant(DEFAULT_END_DATE_TIME))))
            .andExpect(jsonPath("$.[*].passengerAmount").value(hasItem(DEFAULT_PASSENGER_AMOUNT)))
            .andExpect(jsonPath("$.[*].isInvoiced").value(hasItem(DEFAULT_IS_INVOICED.booleanValue())))
            .andExpect(jsonPath("$.[*].paymentAmount").value(hasItem(DEFAULT_PAYMENT_AMOUNT)))
            .andExpect(jsonPath("$.[*].isInvoicePaid").value(hasItem(DEFAULT_IS_INVOICE_PAID.booleanValue())))
            .andExpect(jsonPath("$.[*].paymentDate").value(hasItem(sameInstant(DEFAULT_PAYMENT_DATE))));
    }

    @Test
    @Transactional
    public void getTour() throws Exception {
        // Initialize the database
        tourRepository.saveAndFlush(tour);

        // Get the tour
        restTourMockMvc.perform(get("/api/tours/{id}", tour.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tour.getId().intValue()))
            .andExpect(jsonPath("$.groupNumber").value(DEFAULT_GROUP_NUMBER))
            .andExpect(jsonPath("$.startPlace").value(DEFAULT_START_PLACE.toString()))
            .andExpect(jsonPath("$.startDateTime").value(sameInstant(DEFAULT_START_DATE_TIME)))
            .andExpect(jsonPath("$.endPlace").value(DEFAULT_END_PLACE.toString()))
            .andExpect(jsonPath("$.endDateTime").value(sameInstant(DEFAULT_END_DATE_TIME)))
            .andExpect(jsonPath("$.passengerAmount").value(DEFAULT_PASSENGER_AMOUNT))
            .andExpect(jsonPath("$.isInvoiced").value(DEFAULT_IS_INVOICED.booleanValue()))
            .andExpect(jsonPath("$.paymentAmount").value(DEFAULT_PAYMENT_AMOUNT))
            .andExpect(jsonPath("$.isInvoicePaid").value(DEFAULT_IS_INVOICE_PAID.booleanValue()))
            .andExpect(jsonPath("$.paymentDate").value(sameInstant(DEFAULT_PAYMENT_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingTour() throws Exception {
        // Get the tour
        restTourMockMvc.perform(get("/api/tours/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTour() throws Exception {
        // Initialize the database
        tourRepository.saveAndFlush(tour);
        int databaseSizeBeforeUpdate = tourRepository.findAll().size();

        // Update the tour
        Tour updatedTour = tourRepository.findOne(tour.getId());
        // Disconnect from session so that the updates on updatedTour are not directly saved in db
        em.detach(updatedTour);
        updatedTour
            .groupNumber(UPDATED_GROUP_NUMBER)
            .startPlace(UPDATED_START_PLACE)
            .startDateTime(UPDATED_START_DATE_TIME)
            .endPlace(UPDATED_END_PLACE)
            .endDateTime(UPDATED_END_DATE_TIME)
            .passengerAmount(UPDATED_PASSENGER_AMOUNT)
            .isInvoiced(UPDATED_IS_INVOICED)
            .paymentAmount(UPDATED_PAYMENT_AMOUNT)
            .isInvoicePaid(UPDATED_IS_INVOICE_PAID)
            .paymentDate(UPDATED_PAYMENT_DATE);
        TourDTO tourDTO = tourMapper.toDto(updatedTour);

        restTourMockMvc.perform(put("/api/tours")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tourDTO)))
            .andExpect(status().isOk());

        // Validate the Tour in the database
        List<Tour> tourList = tourRepository.findAll();
        assertThat(tourList).hasSize(databaseSizeBeforeUpdate);
        Tour testTour = tourList.get(tourList.size() - 1);
        assertThat(testTour.getGroupNumber()).isEqualTo(UPDATED_GROUP_NUMBER);
        assertThat(testTour.getStartPlace()).isEqualTo(UPDATED_START_PLACE);
        assertThat(testTour.getStartDateTime()).isEqualTo(UPDATED_START_DATE_TIME);
        assertThat(testTour.getEndPlace()).isEqualTo(UPDATED_END_PLACE);
        assertThat(testTour.getEndDateTime()).isEqualTo(UPDATED_END_DATE_TIME);
        assertThat(testTour.getPassengerAmount()).isEqualTo(UPDATED_PASSENGER_AMOUNT);
        assertThat(testTour.isIsInvoiced()).isEqualTo(UPDATED_IS_INVOICED);
        assertThat(testTour.getPaymentAmount()).isEqualTo(UPDATED_PAYMENT_AMOUNT);
        assertThat(testTour.isIsInvoicePaid()).isEqualTo(UPDATED_IS_INVOICE_PAID);
        assertThat(testTour.getPaymentDate()).isEqualTo(UPDATED_PAYMENT_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingTour() throws Exception {
        int databaseSizeBeforeUpdate = tourRepository.findAll().size();

        // Create the Tour
        TourDTO tourDTO = tourMapper.toDto(tour);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTourMockMvc.perform(put("/api/tours")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tourDTO)))
            .andExpect(status().isCreated());

        // Validate the Tour in the database
        List<Tour> tourList = tourRepository.findAll();
        assertThat(tourList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTour() throws Exception {
        // Initialize the database
        tourRepository.saveAndFlush(tour);
        int databaseSizeBeforeDelete = tourRepository.findAll().size();

        // Get the tour
        restTourMockMvc.perform(delete("/api/tours/{id}", tour.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Tour> tourList = tourRepository.findAll();
        assertThat(tourList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tour.class);
        Tour tour1 = new Tour();
        tour1.setId(1L);
        Tour tour2 = new Tour();
        tour2.setId(tour1.getId());
        assertThat(tour1).isEqualTo(tour2);
        tour2.setId(2L);
        assertThat(tour1).isNotEqualTo(tour2);
        tour1.setId(null);
        assertThat(tour1).isNotEqualTo(tour2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TourDTO.class);
        TourDTO tourDTO1 = new TourDTO();
        tourDTO1.setId(1L);
        TourDTO tourDTO2 = new TourDTO();
        assertThat(tourDTO1).isNotEqualTo(tourDTO2);
        tourDTO2.setId(tourDTO1.getId());
        assertThat(tourDTO1).isEqualTo(tourDTO2);
        tourDTO2.setId(2L);
        assertThat(tourDTO1).isNotEqualTo(tourDTO2);
        tourDTO1.setId(null);
        assertThat(tourDTO1).isNotEqualTo(tourDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tourMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tourMapper.fromId(null)).isNull();
    }
}
