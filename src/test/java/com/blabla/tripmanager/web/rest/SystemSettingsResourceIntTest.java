package com.blabla.tripmanager.web.rest;

import com.blabla.tripmanager.TestApp;

import com.blabla.tripmanager.domain.SystemSettings;
import com.blabla.tripmanager.repository.SystemSettingsRepository;
import com.blabla.tripmanager.service.SystemSettingsService;
import com.blabla.tripmanager.service.dto.SystemSettingsDTO;
import com.blabla.tripmanager.service.mapper.SystemSettingsMapper;
import com.blabla.tripmanager.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.blabla.tripmanager.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SystemSettingsResource REST controller.
 *
 * @see SystemSettingsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class SystemSettingsResourceIntTest {

    private static final Integer DEFAULT_DAYS_TO_NOTIFY = 1;
    private static final Integer UPDATED_DAYS_TO_NOTIFY = 2;

    private static final Integer DEFAULT_DAYS_TO_PICK_BUS = 1;
    private static final Integer UPDATED_DAYS_TO_PICK_BUS = 2;

    private static final Integer DEFAULT_MAX_KMS_GROUP = 1;
    private static final Integer UPDATED_MAX_KMS_GROUP = 2;

    private static final Integer DEFAULT_DAYS_SYSTEM_NOTIFY = 1;
    private static final Integer UPDATED_DAYS_SYSTEM_NOTIFY = 2;

    @Autowired
    private SystemSettingsRepository systemSettingsRepository;

    @Autowired
    private SystemSettingsMapper systemSettingsMapper;

    @Autowired
    private SystemSettingsService systemSettingsService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSystemSettingsMockMvc;

    private SystemSettings systemSettings;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SystemSettingsResource systemSettingsResource = new SystemSettingsResource(systemSettingsService);
        this.restSystemSettingsMockMvc = MockMvcBuilders.standaloneSetup(systemSettingsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SystemSettings createEntity(EntityManager em) {
        SystemSettings systemSettings = new SystemSettings()
            .daysToNotify(DEFAULT_DAYS_TO_NOTIFY)
            .daysToPickBus(DEFAULT_DAYS_TO_PICK_BUS)
            .maxKmsGroup(DEFAULT_MAX_KMS_GROUP)
            .daysSystemNotify(DEFAULT_DAYS_SYSTEM_NOTIFY);
        return systemSettings;
    }

    @Before
    public void initTest() {
        systemSettings = createEntity(em);
    }

    @Test
    @Transactional
    public void createSystemSettings() throws Exception {
        int databaseSizeBeforeCreate = systemSettingsRepository.findAll().size();

        // Create the SystemSettings
        SystemSettingsDTO systemSettingsDTO = systemSettingsMapper.toDto(systemSettings);
        restSystemSettingsMockMvc.perform(post("/api/system-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(systemSettingsDTO)))
            .andExpect(status().isCreated());

        // Validate the SystemSettings in the database
        List<SystemSettings> systemSettingsList = systemSettingsRepository.findAll();
        assertThat(systemSettingsList).hasSize(databaseSizeBeforeCreate + 1);
        SystemSettings testSystemSettings = systemSettingsList.get(systemSettingsList.size() - 1);
        assertThat(testSystemSettings.getDaysToNotify()).isEqualTo(DEFAULT_DAYS_TO_NOTIFY);
        assertThat(testSystemSettings.getDaysToPickBus()).isEqualTo(DEFAULT_DAYS_TO_PICK_BUS);
        assertThat(testSystemSettings.getMaxKmsGroup()).isEqualTo(DEFAULT_MAX_KMS_GROUP);
        assertThat(testSystemSettings.getDaysSystemNotify()).isEqualTo(DEFAULT_DAYS_SYSTEM_NOTIFY);
    }

    @Test
    @Transactional
    public void createSystemSettingsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = systemSettingsRepository.findAll().size();

        // Create the SystemSettings with an existing ID
        systemSettings.setId(1L);
        SystemSettingsDTO systemSettingsDTO = systemSettingsMapper.toDto(systemSettings);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSystemSettingsMockMvc.perform(post("/api/system-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(systemSettingsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SystemSettings in the database
        List<SystemSettings> systemSettingsList = systemSettingsRepository.findAll();
        assertThat(systemSettingsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSystemSettings() throws Exception {
        // Initialize the database
        systemSettingsRepository.saveAndFlush(systemSettings);

        // Get all the systemSettingsList
        restSystemSettingsMockMvc.perform(get("/api/system-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(systemSettings.getId().intValue())))
            .andExpect(jsonPath("$.[*].daysToNotify").value(hasItem(DEFAULT_DAYS_TO_NOTIFY)))
            .andExpect(jsonPath("$.[*].daysToPickBus").value(hasItem(DEFAULT_DAYS_TO_PICK_BUS)))
            .andExpect(jsonPath("$.[*].maxKmsGroup").value(hasItem(DEFAULT_MAX_KMS_GROUP)))
            .andExpect(jsonPath("$.[*].daysSystemNotify").value(hasItem(DEFAULT_DAYS_SYSTEM_NOTIFY)));
    }

    @Test
    @Transactional
    public void getSystemSettings() throws Exception {
        // Initialize the database
        systemSettingsRepository.saveAndFlush(systemSettings);

        // Get the systemSettings
        restSystemSettingsMockMvc.perform(get("/api/system-settings/{id}", systemSettings.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(systemSettings.getId().intValue()))
            .andExpect(jsonPath("$.daysToNotify").value(DEFAULT_DAYS_TO_NOTIFY))
            .andExpect(jsonPath("$.daysToPickBus").value(DEFAULT_DAYS_TO_PICK_BUS))
            .andExpect(jsonPath("$.maxKmsGroup").value(DEFAULT_MAX_KMS_GROUP))
            .andExpect(jsonPath("$.daysSystemNotify").value(DEFAULT_DAYS_SYSTEM_NOTIFY));
    }

    @Test
    @Transactional
    public void getNonExistingSystemSettings() throws Exception {
        // Get the systemSettings
        restSystemSettingsMockMvc.perform(get("/api/system-settings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSystemSettings() throws Exception {
        // Initialize the database
        systemSettingsRepository.saveAndFlush(systemSettings);
        int databaseSizeBeforeUpdate = systemSettingsRepository.findAll().size();

        // Update the systemSettings
        SystemSettings updatedSystemSettings = systemSettingsRepository.findOne(systemSettings.getId());
        // Disconnect from session so that the updates on updatedSystemSettings are not directly saved in db
        em.detach(updatedSystemSettings);
        updatedSystemSettings
            .daysToNotify(UPDATED_DAYS_TO_NOTIFY)
            .daysToPickBus(UPDATED_DAYS_TO_PICK_BUS)
            .maxKmsGroup(UPDATED_MAX_KMS_GROUP)
            .daysSystemNotify(UPDATED_DAYS_SYSTEM_NOTIFY);
        SystemSettingsDTO systemSettingsDTO = systemSettingsMapper.toDto(updatedSystemSettings);

        restSystemSettingsMockMvc.perform(put("/api/system-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(systemSettingsDTO)))
            .andExpect(status().isOk());

        // Validate the SystemSettings in the database
        List<SystemSettings> systemSettingsList = systemSettingsRepository.findAll();
        assertThat(systemSettingsList).hasSize(databaseSizeBeforeUpdate);
        SystemSettings testSystemSettings = systemSettingsList.get(systemSettingsList.size() - 1);
        assertThat(testSystemSettings.getDaysToNotify()).isEqualTo(UPDATED_DAYS_TO_NOTIFY);
        assertThat(testSystemSettings.getDaysToPickBus()).isEqualTo(UPDATED_DAYS_TO_PICK_BUS);
        assertThat(testSystemSettings.getMaxKmsGroup()).isEqualTo(UPDATED_MAX_KMS_GROUP);
        assertThat(testSystemSettings.getDaysSystemNotify()).isEqualTo(UPDATED_DAYS_SYSTEM_NOTIFY);
    }

    @Test
    @Transactional
    public void updateNonExistingSystemSettings() throws Exception {
        int databaseSizeBeforeUpdate = systemSettingsRepository.findAll().size();

        // Create the SystemSettings
        SystemSettingsDTO systemSettingsDTO = systemSettingsMapper.toDto(systemSettings);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSystemSettingsMockMvc.perform(put("/api/system-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(systemSettingsDTO)))
            .andExpect(status().isCreated());

        // Validate the SystemSettings in the database
        List<SystemSettings> systemSettingsList = systemSettingsRepository.findAll();
        assertThat(systemSettingsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSystemSettings() throws Exception {
        // Initialize the database
        systemSettingsRepository.saveAndFlush(systemSettings);
        int databaseSizeBeforeDelete = systemSettingsRepository.findAll().size();

        // Get the systemSettings
        restSystemSettingsMockMvc.perform(delete("/api/system-settings/{id}", systemSettings.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SystemSettings> systemSettingsList = systemSettingsRepository.findAll();
        assertThat(systemSettingsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SystemSettings.class);
        SystemSettings systemSettings1 = new SystemSettings();
        systemSettings1.setId(1L);
        SystemSettings systemSettings2 = new SystemSettings();
        systemSettings2.setId(systemSettings1.getId());
        assertThat(systemSettings1).isEqualTo(systemSettings2);
        systemSettings2.setId(2L);
        assertThat(systemSettings1).isNotEqualTo(systemSettings2);
        systemSettings1.setId(null);
        assertThat(systemSettings1).isNotEqualTo(systemSettings2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SystemSettingsDTO.class);
        SystemSettingsDTO systemSettingsDTO1 = new SystemSettingsDTO();
        systemSettingsDTO1.setId(1L);
        SystemSettingsDTO systemSettingsDTO2 = new SystemSettingsDTO();
        assertThat(systemSettingsDTO1).isNotEqualTo(systemSettingsDTO2);
        systemSettingsDTO2.setId(systemSettingsDTO1.getId());
        assertThat(systemSettingsDTO1).isEqualTo(systemSettingsDTO2);
        systemSettingsDTO2.setId(2L);
        assertThat(systemSettingsDTO1).isNotEqualTo(systemSettingsDTO2);
        systemSettingsDTO1.setId(null);
        assertThat(systemSettingsDTO1).isNotEqualTo(systemSettingsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(systemSettingsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(systemSettingsMapper.fromId(null)).isNull();
    }
}
