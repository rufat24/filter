package com.blabla.tripmanager.web.rest;

import com.blabla.tripmanager.TestApp;

import com.blabla.tripmanager.domain.Bus;
import com.blabla.tripmanager.repository.BusRepository;
import com.blabla.tripmanager.service.BusService;
import com.blabla.tripmanager.service.dto.BusDTO;
import com.blabla.tripmanager.service.mapper.BusMapper;
import com.blabla.tripmanager.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.blabla.tripmanager.web.rest.TestUtil.sameInstant;
import static com.blabla.tripmanager.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BusResource REST controller.
 *
 * @see BusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class BusResourceIntTest {

    private static final String DEFAULT_BUS_MODEL = "AAAAAAAAAA";
    private static final String UPDATED_BUS_MODEL = "BBBBBBBBBB";

    private static final String DEFAULT_REGISTRATION_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_REGISTRATION_NUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_YEAR_OF_PRODUCTION = 1;
    private static final Integer UPDATED_YEAR_OF_PRODUCTION = 2;

    private static final ZonedDateTime DEFAULT_TACHOGRAPH_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TACHOGRAPH_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private BusRepository busRepository;

    @Autowired
    private BusMapper busMapper;

    @Autowired
    private BusService busService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBusMockMvc;

    private Bus bus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BusResource busResource = new BusResource(busService);
        this.restBusMockMvc = MockMvcBuilders.standaloneSetup(busResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bus createEntity(EntityManager em) {
        Bus bus = new Bus()
            .busModel(DEFAULT_BUS_MODEL)
            .registrationNumber(DEFAULT_REGISTRATION_NUMBER)
            .yearOfProduction(DEFAULT_YEAR_OF_PRODUCTION)
            .tachographDate(DEFAULT_TACHOGRAPH_DATE);
        return bus;
    }

    @Before
    public void initTest() {
        bus = createEntity(em);
    }

    @Test
    @Transactional
    public void createBus() throws Exception {
        int databaseSizeBeforeCreate = busRepository.findAll().size();

        // Create the Bus
        BusDTO busDTO = busMapper.toDto(bus);
        restBusMockMvc.perform(post("/api/buses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busDTO)))
            .andExpect(status().isCreated());

        // Validate the Bus in the database
        List<Bus> busList = busRepository.findAll();
        assertThat(busList).hasSize(databaseSizeBeforeCreate + 1);
        Bus testBus = busList.get(busList.size() - 1);
        assertThat(testBus.getBusModel()).isEqualTo(DEFAULT_BUS_MODEL);
        assertThat(testBus.getRegistrationNumber()).isEqualTo(DEFAULT_REGISTRATION_NUMBER);
        assertThat(testBus.getYearOfProduction()).isEqualTo(DEFAULT_YEAR_OF_PRODUCTION);
        assertThat(testBus.getTachographDate()).isEqualTo(DEFAULT_TACHOGRAPH_DATE);
    }

    @Test
    @Transactional
    public void createBusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = busRepository.findAll().size();

        // Create the Bus with an existing ID
        bus.setId(1L);
        BusDTO busDTO = busMapper.toDto(bus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBusMockMvc.perform(post("/api/buses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bus in the database
        List<Bus> busList = busRepository.findAll();
        assertThat(busList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBuses() throws Exception {
        // Initialize the database
        busRepository.saveAndFlush(bus);

        // Get all the busList
        restBusMockMvc.perform(get("/api/buses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bus.getId().intValue())))
            .andExpect(jsonPath("$.[*].busModel").value(hasItem(DEFAULT_BUS_MODEL.toString())))
            .andExpect(jsonPath("$.[*].registrationNumber").value(hasItem(DEFAULT_REGISTRATION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].yearOfProduction").value(hasItem(DEFAULT_YEAR_OF_PRODUCTION)))
            .andExpect(jsonPath("$.[*].tachographDate").value(hasItem(sameInstant(DEFAULT_TACHOGRAPH_DATE))));
    }

    @Test
    @Transactional
    public void getBus() throws Exception {
        // Initialize the database
        busRepository.saveAndFlush(bus);

        // Get the bus
        restBusMockMvc.perform(get("/api/buses/{id}", bus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bus.getId().intValue()))
            .andExpect(jsonPath("$.busModel").value(DEFAULT_BUS_MODEL.toString()))
            .andExpect(jsonPath("$.registrationNumber").value(DEFAULT_REGISTRATION_NUMBER.toString()))
            .andExpect(jsonPath("$.yearOfProduction").value(DEFAULT_YEAR_OF_PRODUCTION))
            .andExpect(jsonPath("$.tachographDate").value(sameInstant(DEFAULT_TACHOGRAPH_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingBus() throws Exception {
        // Get the bus
        restBusMockMvc.perform(get("/api/buses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBus() throws Exception {
        // Initialize the database
        busRepository.saveAndFlush(bus);
        int databaseSizeBeforeUpdate = busRepository.findAll().size();

        // Update the bus
        Bus updatedBus = busRepository.findOne(bus.getId());
        // Disconnect from session so that the updates on updatedBus are not directly saved in db
        em.detach(updatedBus);
        updatedBus
            .busModel(UPDATED_BUS_MODEL)
            .registrationNumber(UPDATED_REGISTRATION_NUMBER)
            .yearOfProduction(UPDATED_YEAR_OF_PRODUCTION)
            .tachographDate(UPDATED_TACHOGRAPH_DATE);
        BusDTO busDTO = busMapper.toDto(updatedBus);

        restBusMockMvc.perform(put("/api/buses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busDTO)))
            .andExpect(status().isOk());

        // Validate the Bus in the database
        List<Bus> busList = busRepository.findAll();
        assertThat(busList).hasSize(databaseSizeBeforeUpdate);
        Bus testBus = busList.get(busList.size() - 1);
        assertThat(testBus.getBusModel()).isEqualTo(UPDATED_BUS_MODEL);
        assertThat(testBus.getRegistrationNumber()).isEqualTo(UPDATED_REGISTRATION_NUMBER);
        assertThat(testBus.getYearOfProduction()).isEqualTo(UPDATED_YEAR_OF_PRODUCTION);
        assertThat(testBus.getTachographDate()).isEqualTo(UPDATED_TACHOGRAPH_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingBus() throws Exception {
        int databaseSizeBeforeUpdate = busRepository.findAll().size();

        // Create the Bus
        BusDTO busDTO = busMapper.toDto(bus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBusMockMvc.perform(put("/api/buses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(busDTO)))
            .andExpect(status().isCreated());

        // Validate the Bus in the database
        List<Bus> busList = busRepository.findAll();
        assertThat(busList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBus() throws Exception {
        // Initialize the database
        busRepository.saveAndFlush(bus);
        int databaseSizeBeforeDelete = busRepository.findAll().size();

        // Get the bus
        restBusMockMvc.perform(delete("/api/buses/{id}", bus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Bus> busList = busRepository.findAll();
        assertThat(busList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Bus.class);
        Bus bus1 = new Bus();
        bus1.setId(1L);
        Bus bus2 = new Bus();
        bus2.setId(bus1.getId());
        assertThat(bus1).isEqualTo(bus2);
        bus2.setId(2L);
        assertThat(bus1).isNotEqualTo(bus2);
        bus1.setId(null);
        assertThat(bus1).isNotEqualTo(bus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BusDTO.class);
        BusDTO busDTO1 = new BusDTO();
        busDTO1.setId(1L);
        BusDTO busDTO2 = new BusDTO();
        assertThat(busDTO1).isNotEqualTo(busDTO2);
        busDTO2.setId(busDTO1.getId());
        assertThat(busDTO1).isEqualTo(busDTO2);
        busDTO2.setId(2L);
        assertThat(busDTO1).isNotEqualTo(busDTO2);
        busDTO1.setId(null);
        assertThat(busDTO1).isNotEqualTo(busDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(busMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(busMapper.fromId(null)).isNull();
    }
}
