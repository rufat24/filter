package com.blabla.tripmanager.util;


import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Created by gismat on 05.11.2017.
 */
public class HashingUtil {


    public static String imageNameMd5hashing(String text)
    {
        String hashtext=null;
        try
        {
            text=(System.currentTimeMillis() % 1000)+text;
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(text.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            hashtext = bigInt.toString(16);
            while(hashtext.length() < 32 ){
                hashtext = "0"+hashtext;
            }
        } catch (Exception e1)
        {
        }
        return hashtext;
    }
}
