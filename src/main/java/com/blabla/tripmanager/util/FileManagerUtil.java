package com.blabla.tripmanager.util;


/*import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.util.Base64;*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by TuralHa on 28-Oct-17.
 */
public class FileManagerUtil {

    public static final Logger log = LoggerFactory.getLogger(FileManagerUtil.class);
    public static final String FOLDER_NAME="documents";

    public static void uploadImage(byte[] image, String hashedFileName){
        Path file = Paths.get("."+File.separator+FOLDER_NAME+File.separator+hashedFileName);
        try {
            Files.write(file, image);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static void uploadImage(MultipartFile image, String hashedFileName){
        Path file = Paths.get("."+File.separator+FOLDER_NAME+File.separator+hashedFileName);
        try {
            Files.copy(image.getInputStream(), file);
        } catch (IOException e) {
            e.printStackTrace();
            }
    }

    public static byte[] downloadImage(String hashedFileName){
        Path file = Paths.get("."+File.separator+FOLDER_NAME+File.separator+hashedFileName);
        byte[] tempFile = new byte[100000];
        try {
            tempFile = Files.readAllBytes(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tempFile;
    }

/*
    private static String ftpServer = "localhost";

    private static int ftpPort = 21;

    private static String ftpUser = "ftpuser";

    private static String ftpPswrd = "password";

    private static String ftpFilePath = "/path/to/folder/";


    public static void uploadImageFTP(byte[] image, String hashedFileName) {
            FTPClient ftpClient = new FTPClient();
            try {
                log.debug("server: {}, port: {}", ftpServer, ftpPort);
                log.debug("username: {}, password: {}", ftpUser, ftpPswrd);
                ftpClient.connect(ftpServer, ftpPort);
                ftpClient.login(ftpUser, ftpPswrd);
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    log.info("Start Uploading File: " + hashedFileName);
                    String fileName = ftpFilePath + hashedFileName;
                    log.debug("filename: {}", fileName);
                    OutputStream outputStream = ftpClient.storeFileStream(fileName);
                    outputStream.write(image);
                    outputStream.close();
                    boolean completed = ftpClient.completePendingCommand();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeFTPConnection(ftpClient);
            }

        }

    public static File downloadFile(String imageHash) {
        FTPClient ftpClient = new FTPClient();
        byte[] bytesArray;
        try {
            log.debug("server: {}, port: {}", ftpServer, ftpPort);
            log.debug("username: {}, password: {}", ftpUser, ftpPswrd);
            ftpClient.connect(ftpServer, ftpPort);
            ftpClient.login(ftpUser, ftpPswrd);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            String absolutePath = ftpFilePath + imageHash;
            log.info("Start Downloading File: " + imageHash);
            OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(imageHash));
            InputStream inputStream = ftpClient.retrieveFileStream(absolutePath);

            bytesArray = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(bytesArray)) != -1) {
                outputStream2.write(bytesArray, 0, bytesRead);
            }

            boolean success = ftpClient.completePendingCommand();
            if (success) {
                log.info("File has been downloaded successfully.");
            }
            outputStream2.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            //throw new IqrexException(AppStatus.FILE_DOWNLOAD_ERROR);
        } finally {
            closeFTPConnection(ftpClient);
        }
//        new File(imageHash).delete();
        return new File(imageHash);
    }

    private static void closeFTPConnection(FTPClient ftpClient){
        try {
            if (ftpClient.isConnected()) {
                ftpClient.logout();
                ftpClient.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String encodeFileToBase64Binary(File file) throws IOException {
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        return new String(encoded, StandardCharsets.US_ASCII);
    }*/

}
