package com.blabla.tripmanager.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SystemSettings entity.
 */
public class SystemSettingsDTO implements Serializable {

    private Long id;

    private Integer daysToNotify;

    private Integer daysToPickBus;

    private Integer maxKmsGroup;

    private Integer daysSystemNotify;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDaysToNotify() {
        return daysToNotify;
    }

    public void setDaysToNotify(Integer daysToNotify) {
        this.daysToNotify = daysToNotify;
    }

    public Integer getDaysToPickBus() {
        return daysToPickBus;
    }

    public void setDaysToPickBus(Integer daysToPickBus) {
        this.daysToPickBus = daysToPickBus;
    }

    public Integer getMaxKmsGroup() {
        return maxKmsGroup;
    }

    public void setMaxKmsGroup(Integer maxKmsGroup) {
        this.maxKmsGroup = maxKmsGroup;
    }

    public Integer getDaysSystemNotify() {
        return daysSystemNotify;
    }

    public void setDaysSystemNotify(Integer daysSystemNotify) {
        this.daysSystemNotify = daysSystemNotify;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SystemSettingsDTO systemSettingsDTO = (SystemSettingsDTO) o;
        if(systemSettingsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), systemSettingsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SystemSettingsDTO{" +
            "id=" + getId() +
            ", daysToNotify=" + getDaysToNotify() +
            ", daysToPickBus=" + getDaysToPickBus() +
            ", maxKmsGroup=" + getMaxKmsGroup() +
            ", daysSystemNotify=" + getDaysSystemNotify() +
            "}";
    }
}
