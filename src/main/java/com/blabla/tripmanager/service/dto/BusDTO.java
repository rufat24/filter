package com.blabla.tripmanager.service.dto;


import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Bus entity.
 */
public class BusDTO implements Serializable {

    private Long id;

    private String busModel;

    private String registrationNumber;

    private Integer yearOfProduction;

    private ZonedDateTime tachographDate;

    private Set<DriverDTO> drivers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusModel() {
        return busModel;
    }

    public void setBusModel(String busModel) {
        this.busModel = busModel;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(Integer yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public ZonedDateTime getTachographDate() {
        return tachographDate;
    }

    public void setTachographDate(ZonedDateTime tachographDate) {
        this.tachographDate = tachographDate;
    }

    public Set<DriverDTO> getDrivers() {
        return drivers;
    }

    public void setDrivers(Set<DriverDTO> drivers) {
        this.drivers = drivers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BusDTO busDTO = (BusDTO) o;
        if(busDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), busDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BusDTO{" +
            "id=" + getId() +
            ", busModel='" + getBusModel() + "'" +
            ", registrationNumber='" + getRegistrationNumber() + "'" +
            ", yearOfProduction=" + getYearOfProduction() +
            ", tachographDate='" + getTachographDate() + "'" +
            "}";
    }
}
