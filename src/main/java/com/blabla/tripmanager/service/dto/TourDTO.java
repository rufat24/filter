package com.blabla.tripmanager.service.dto;


import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Tour entity.
 */
public class TourDTO implements Serializable {

    private Long id;

    private Integer groupNumber;

    private String startPlace;

    private ZonedDateTime startDateTime;

    private String endPlace;

    private ZonedDateTime endDateTime;

    private Integer passengerAmount;

    private Boolean isInvoiced;

    private Integer paymentAmount;

    private Boolean isInvoicePaid;

    private ZonedDateTime paymentDate;

    private Long companyId;

    private BusDTO busId;

    public void setBusId(BusDTO busId) {
        this.busId = busId;
    }

    private Long driverId;

    private Long ownerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public ZonedDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(ZonedDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public ZonedDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(ZonedDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Integer getPassengerAmount() {
        return passengerAmount;
    }

    public void setPassengerAmount(Integer passengerAmount) {
        this.passengerAmount = passengerAmount;
    }

    public Boolean isIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(Boolean isInvoiced) {
        this.isInvoiced = isInvoiced;
    }

    public Integer getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Integer paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Boolean isIsInvoicePaid() {
        return isInvoicePaid;
    }

    public void setIsInvoicePaid(Boolean isInvoicePaid) {
        this.isInvoicePaid = isInvoicePaid;
    }

    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(ZonedDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long userId) {
        this.ownerId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TourDTO tourDTO = (TourDTO) o;
        if(tourDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tourDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TourDTO{" +
            "id=" + getId() +
            ", groupNumber=" + getGroupNumber() +
            ", startPlace='" + getStartPlace() + "'" +
            ", startDateTime='" + getStartDateTime() + "'" +
            ", endPlace='" + getEndPlace() + "'" +
            ", endDateTime='" + getEndDateTime() + "'" +
            ", passengerAmount=" + getPassengerAmount() +
            ", isInvoiced='" + isIsInvoiced() + "'" +
            ", paymentAmount=" + getPaymentAmount() +
            ", isInvoicePaid='" + isIsInvoicePaid() + "'" +
            ", paymentDate='" + getPaymentDate() + "'" +
            "}";
    }
}
