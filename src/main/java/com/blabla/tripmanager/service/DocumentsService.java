package com.blabla.tripmanager.service;

import com.blabla.tripmanager.service.dto.DocumentsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
/**
 * Service Interface for managing Documents.
 */
public interface DocumentsService {

    /**
     * Save a documents.
     *
     * @param documentsDTO the entity to save
     * @return the persisted entity
     */
    DocumentsDTO save(DocumentsDTO documentsDTO);
    DocumentsDTO saveFile(Long id, MultipartFile document);
    /**
     * Get all the documents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DocumentsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" documents.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DocumentsDTO findOne(Long id);

    /**
     * Delete the "id" documents.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
