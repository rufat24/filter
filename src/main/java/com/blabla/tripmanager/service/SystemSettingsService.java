package com.blabla.tripmanager.service;

import com.blabla.tripmanager.service.dto.SystemSettingsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing SystemSettings.
 */
public interface SystemSettingsService {

    /**
     * Save a systemSettings.
     *
     * @param systemSettingsDTO the entity to save
     * @return the persisted entity
     */
    SystemSettingsDTO save(SystemSettingsDTO systemSettingsDTO);

    /**
     * Get all the systemSettings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SystemSettingsDTO> findAll(Pageable pageable);

    /**
     * Get the "id" systemSettings.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SystemSettingsDTO findOne(Long id);

    /**
     * Delete the "id" systemSettings.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
