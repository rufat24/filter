package com.blabla.tripmanager.service.impl;

import com.blabla.tripmanager.service.BusService;
import com.blabla.tripmanager.domain.Bus;
import com.blabla.tripmanager.repository.BusRepository;
import com.blabla.tripmanager.service.dto.BusDTO;
import com.blabla.tripmanager.service.mapper.BusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Bus.
 */
@Service
@Transactional
public class BusServiceImpl implements BusService {

    private final Logger log = LoggerFactory.getLogger(BusServiceImpl.class);

    private final BusRepository busRepository;

    private final BusMapper busMapper;

    public BusServiceImpl(BusRepository busRepository, BusMapper busMapper) {
        this.busRepository = busRepository;
        this.busMapper = busMapper;
    }

    /**
     * Save a bus.
     *
     * @param busDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BusDTO save(BusDTO busDTO) {
        log.debug("Request to save Bus : {}", busDTO);
        Bus bus = busMapper.toEntity(busDTO);
        bus = busRepository.save(bus);
        return busMapper.toDto(bus);
    }

    /**
     * Get all the buses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Buses");
        return busRepository.findAll(pageable)
            .map(busMapper::toDto);
    }

    /**
     * Get one bus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public BusDTO findOne(Long id) {
        log.debug("Request to get Bus : {}", id);
        Bus bus = busRepository.findOneWithEagerRelationships(id);
        return busMapper.toDto(bus);
    }

    /**
     * Delete the bus by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Bus : {}", id);
        busRepository.delete(id);
    }
}
