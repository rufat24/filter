package com.blabla.tripmanager.service.impl;

import com.blabla.tripmanager.service.DriverService;
import com.blabla.tripmanager.domain.Driver;
import com.blabla.tripmanager.repository.DriverRepository;
import com.blabla.tripmanager.service.dto.DriverDTO;
import com.blabla.tripmanager.service.mapper.DriverMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.blabla.tripmanager.util.HashingUtil;
import com.blabla.tripmanager.util.FileManagerUtil;
import liquibase.util.file.FilenameUtils;
/**
 * Service Implementation for managing Driver.
 */
@Service
@Transactional
public class DriverServiceImpl implements DriverService {

    private final Logger log = LoggerFactory.getLogger(DriverServiceImpl.class);

    private final DriverRepository driverRepository;

    private final DriverMapper driverMapper;

    public DriverServiceImpl(DriverRepository driverRepository, DriverMapper driverMapper) {
        this.driverRepository = driverRepository;
        this.driverMapper = driverMapper;
    }

    /**
     * Save a driver.
     *
     * @param driverDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DriverDTO save(DriverDTO driverDTO) {
        log.debug("Request to save Driver : {}", driverDTO);
        Driver driver = driverMapper.toEntity(driverDTO);
        driver = driverRepository.save(driver);
        return driverMapper.toDto(driver);
    }

    /**
     * Get all the drivers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DriverDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Drivers");
        return driverRepository.findAll(pageable)
            .map(driverMapper::toDto);
    }
    public DriverDTO saveFile(Long id, MultipartFile image) {
        Driver driver = driverRepository.findOne(id);
        String hashedImageName = HashingUtil.imageNameMd5hashing(image.getOriginalFilename())+"."+ FilenameUtils.getExtension(image.getOriginalFilename());
        FileManagerUtil.uploadImage(image, hashedImageName);
        driver.setImage(hashedImageName);
        driver = driverRepository.save(driver);
        return driverMapper.toDto(driver);
    }


    /**
     * Get one driver by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DriverDTO findOne(Long id) {
        log.debug("Request to get Driver : {}", id);
        Driver driver = driverRepository.findOne(id);
        return driverMapper.toDto(driver);
    }

    /**
     * Delete the driver by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Driver : {}", id);
        driverRepository.delete(id);
    }
}
