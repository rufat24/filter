package com.blabla.tripmanager.service.impl;

import com.blabla.tripmanager.service.TourService;
import com.blabla.tripmanager.domain.Tour;
import com.blabla.tripmanager.repository.TourRepository;
import com.blabla.tripmanager.service.dto.TourDTO;
import com.blabla.tripmanager.service.mapper.TourMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.ZonedDateTime;
import java.util.List;
import com.blabla.tripmanager.domain.Company;
import com.blabla.tripmanager.domain.Bus;
import com.blabla.tripmanager.domain.Driver;

/**
 * Service Implementation for managing Tour.
 */
@Service
@Transactional
public class TourServiceImpl implements TourService {

    private final Logger log = LoggerFactory.getLogger(TourServiceImpl.class);

    private final TourRepository tourRepository;

    private final TourMapper tourMapper;

    public TourServiceImpl(TourRepository tourRepository, TourMapper tourMapper) {
        this.tourRepository = tourRepository;
        this.tourMapper = tourMapper;
    }

    /**
     * Save a tour.
     *
     * @param tourDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TourDTO save(TourDTO tourDTO) {
        log.debug("Request to save Tour : {}", tourDTO);
        Tour tour = tourMapper.toEntity(tourDTO);
        tour = tourRepository.save(tour);
        return tourMapper.toDto(tour);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TourDTO> findAllFilter(Company c, Bus b, Driver d, Long id,String splace,String eplace, ZonedDateTime sdate, ZonedDateTime edate,Boolean ii, Boolean ip, Boolean p) {
        return tourMapper.toDto(tourRepository.findAllFilter(c,b,d,id,splace,eplace,sdate,edate,ii,ip,p));
    }

    /**
     * Get all the tours.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TourDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Tours");
        return tourRepository.findAll(pageable)
            .map(tourMapper::toDto);
    }

    /**
     * Get one tour by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TourDTO findOne(Long id) {
        log.debug("Request to get Tour : {}", id);
        Tour tour = tourRepository.findOne(id);
        return tourMapper.toDto(tour);
    }

    /**
     * Delete the tour by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tour : {}", id);
        tourRepository.delete(id);
    }
}
