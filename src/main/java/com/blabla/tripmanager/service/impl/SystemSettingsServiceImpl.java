package com.blabla.tripmanager.service.impl;

import com.blabla.tripmanager.service.SystemSettingsService;
import com.blabla.tripmanager.domain.SystemSettings;
import com.blabla.tripmanager.repository.SystemSettingsRepository;
import com.blabla.tripmanager.service.dto.SystemSettingsDTO;
import com.blabla.tripmanager.service.mapper.SystemSettingsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing SystemSettings.
 */
@Service
@Transactional
public class SystemSettingsServiceImpl implements SystemSettingsService {

    private final Logger log = LoggerFactory.getLogger(SystemSettingsServiceImpl.class);

    private final SystemSettingsRepository systemSettingsRepository;

    private final SystemSettingsMapper systemSettingsMapper;

    public SystemSettingsServiceImpl(SystemSettingsRepository systemSettingsRepository, SystemSettingsMapper systemSettingsMapper) {
        this.systemSettingsRepository = systemSettingsRepository;
        this.systemSettingsMapper = systemSettingsMapper;
    }

    /**
     * Save a systemSettings.
     *
     * @param systemSettingsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SystemSettingsDTO save(SystemSettingsDTO systemSettingsDTO) {
        log.debug("Request to save SystemSettings : {}", systemSettingsDTO);
        SystemSettings systemSettings = systemSettingsMapper.toEntity(systemSettingsDTO);
        systemSettings = systemSettingsRepository.save(systemSettings);
        return systemSettingsMapper.toDto(systemSettings);
    }

    /**
     * Get all the systemSettings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SystemSettingsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SystemSettings");
        return systemSettingsRepository.findAll(pageable)
            .map(systemSettingsMapper::toDto);
    }

    /**
     * Get one systemSettings by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SystemSettingsDTO findOne(Long id) {
        log.debug("Request to get SystemSettings : {}", id);
        SystemSettings systemSettings = systemSettingsRepository.findOne(id);
        return systemSettingsMapper.toDto(systemSettings);
    }

    /**
     * Delete the systemSettings by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SystemSettings : {}", id);
        systemSettingsRepository.delete(id);
    }
}
