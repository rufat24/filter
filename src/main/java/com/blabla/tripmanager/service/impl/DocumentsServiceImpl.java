package com.blabla.tripmanager.service.impl;

import com.blabla.tripmanager.service.DocumentsService;
import com.blabla.tripmanager.domain.Documents;
import com.blabla.tripmanager.repository.DocumentsRepository;
import com.blabla.tripmanager.service.dto.DocumentsDTO;
import com.blabla.tripmanager.service.mapper.DocumentsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.blabla.tripmanager.util.HashingUtil;
import com.blabla.tripmanager.util.FileManagerUtil;
import liquibase.util.file.FilenameUtils;
/**
 * Service Implementation for managing Documents.
 */
@Service
@Transactional
public class DocumentsServiceImpl implements DocumentsService {

    private final Logger log = LoggerFactory.getLogger(DocumentsServiceImpl.class);

    private final DocumentsRepository documentsRepository;

    private final DocumentsMapper documentsMapper;

    public DocumentsServiceImpl(DocumentsRepository documentsRepository, DocumentsMapper documentsMapper) {
        this.documentsRepository = documentsRepository;
        this.documentsMapper = documentsMapper;
    }

    /**
     * Save a documents.
     *
     * @param documentsDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DocumentsDTO save(DocumentsDTO documentsDTO) {
        log.debug("Request to save Documents : {}", documentsDTO);
        Documents documents = documentsMapper.toEntity(documentsDTO);
        documents = documentsRepository.save(documents);
        return documentsMapper.toDto(documents);
    }

    /**
     * Get all the documents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DocumentsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Documents");
        return documentsRepository.findAll(pageable)
            .map(documentsMapper::toDto);
    }

    /**
     * Get one documents by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DocumentsDTO findOne(Long id) {
        log.debug("Request to get Documents : {}", id);
        Documents documents = documentsRepository.findOne(id);
        return documentsMapper.toDto(documents);
    }

    /**
     * Delete the documents by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Documents : {}", id);
        documentsRepository.delete(id);
    }
    @Override
    public DocumentsDTO saveFile(Long id, MultipartFile document) {
        Documents doc=documentsRepository.findOne(id);
        String hashedDocumentName = HashingUtil.imageNameMd5hashing(document.getOriginalFilename())+"."+ FilenameUtils.getExtension(document.getOriginalFilename());
        FileManagerUtil.uploadImage(document, hashedDocumentName);
        doc.setFileHash(hashedDocumentName);
        doc = documentsRepository.save(doc);

        return documentsMapper.toDto(doc);
    }

}
