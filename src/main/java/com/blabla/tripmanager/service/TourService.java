package com.blabla.tripmanager.service;

import com.blabla.tripmanager.domain.Company;
import com.blabla.tripmanager.service.dto.TourDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.time.ZonedDateTime;
import java.util.List;
import com.blabla.tripmanager.domain.Company;
import com.blabla.tripmanager.domain.Driver;
import com.blabla.tripmanager.domain.Bus;
/**
 * Service Interface for managing Tour.
 */
public interface TourService {

    /**
     * Save a tour.
     *
     * @param tourDTO the entity to save
     * @return the persisted entity
     */
    TourDTO save(TourDTO tourDTO);

    /**
     * Get all the tours.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<TourDTO> findAll(Pageable pageable);

    /**
     * Get the "id" tour.
     *
     * @param id the id of the entity
     * @return the entity
     */
    TourDTO findOne(Long id);
    List<TourDTO> findAllFilter(Company c,Bus b, Driver d, Long id,String splace,String eplace,ZonedDateTime sdate, ZonedDateTime edate,Boolean ii, Boolean ip, Boolean p);
    /**
     * Delete the "id" tour.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
