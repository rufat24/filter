package com.blabla.tripmanager.service.mapper;

import com.blabla.tripmanager.domain.*;
import com.blabla.tripmanager.service.dto.NotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Notification and its DTO NotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, TourMapper.class})
public interface NotificationMapper extends EntityMapper<NotificationDTO, Notification> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "tour.id", target = "tourId")
    NotificationDTO toDto(Notification notification);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "tourId", target = "tour")
    Notification toEntity(NotificationDTO notificationDTO);

    default Notification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Notification notification = new Notification();
        notification.setId(id);
        return notification;
    }
}
