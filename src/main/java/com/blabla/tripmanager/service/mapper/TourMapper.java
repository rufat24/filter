package com.blabla.tripmanager.service.mapper;

import com.blabla.tripmanager.domain.*;
import com.blabla.tripmanager.service.dto.TourDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Tour and its DTO TourDTO.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class, BusMapper.class, DriverMapper.class, UserMapper.class})
public interface TourMapper extends EntityMapper<TourDTO, Tour> {

    /*@Mapping(source = "company.id", target = "companyId")
    @Mapping(source = "bus.id", target = "busId")
    @Mapping(source = "driver.id", target = "driverId")*/
    @Mapping(source = "owner.id", target = "ownerId")
    TourDTO toDto(Tour tour);

    /*@Mapping(source = "companyId", target = "company")
    @Mapping(source = "busId", target = "bus")
    @Mapping(source = "driverId", target = "driver")*/
    @Mapping(source = "ownerId", target = "owner")
    Tour toEntity(TourDTO tourDTO);

    default Tour fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tour tour = new Tour();
        tour.setId(id);
        return tour;
    }
}
