package com.blabla.tripmanager.service.mapper;

import com.blabla.tripmanager.domain.*;
import com.blabla.tripmanager.service.dto.DocumentsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Documents and its DTO DocumentsDTO.
 */
@Mapper(componentModel = "spring", uses = {TourMapper.class})
public interface DocumentsMapper extends EntityMapper<DocumentsDTO, Documents> {

    @Mapping(source = "tour.id", target = "tourId")
    DocumentsDTO toDto(Documents documents);

    @Mapping(source = "tourId", target = "tour")
    Documents toEntity(DocumentsDTO documentsDTO);

    default Documents fromId(Long id) {
        if (id == null) {
            return null;
        }
        Documents documents = new Documents();
        documents.setId(id);
        return documents;
    }
}
