package com.blabla.tripmanager.service.mapper;

import com.blabla.tripmanager.domain.*;
import com.blabla.tripmanager.service.dto.SystemSettingsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SystemSettings and its DTO SystemSettingsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SystemSettingsMapper extends EntityMapper<SystemSettingsDTO, SystemSettings> {



    default SystemSettings fromId(Long id) {
        if (id == null) {
            return null;
        }
        SystemSettings systemSettings = new SystemSettings();
        systemSettings.setId(id);
        return systemSettings;
    }
}
