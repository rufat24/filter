package com.blabla.tripmanager.service.mapper;

import com.blabla.tripmanager.domain.*;
import com.blabla.tripmanager.service.dto.BusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Bus and its DTO BusDTO.
 */
@Mapper(componentModel = "spring", uses = {DriverMapper.class})
public interface BusMapper extends EntityMapper<BusDTO, Bus> {



    default Bus fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bus bus = new Bus();
        bus.setId(id);
        return bus;
    }
}
