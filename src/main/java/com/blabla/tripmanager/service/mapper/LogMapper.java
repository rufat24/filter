package com.blabla.tripmanager.service.mapper;

import com.blabla.tripmanager.domain.*;
import com.blabla.tripmanager.service.dto.LogDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Log and its DTO LogDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface LogMapper extends EntityMapper<LogDTO, Log> {

    @Mapping(source = "user.id", target = "userId")
    LogDTO toDto(Log log);

    @Mapping(source = "userId", target = "user")
    Log toEntity(LogDTO logDTO);

    default Log fromId(Long id) {
        if (id == null) {
            return null;
        }
        Log log = new Log();
        log.setId(id);
        return log;
    }
}
