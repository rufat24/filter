package com.blabla.tripmanager.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String EDITOR = "ROLE_EDITOR";

    public static final String SUBEDITOR = "ROLE_SUBEDITOR";

    public static final String INVEDITOR = "ROLE_INVOICE_ED";

    public static final String INVSUBEDITOR = "ROLE_INVOICE_SUBED";

    private AuthoritiesConstants() {
    }
}
