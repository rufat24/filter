package com.blabla.tripmanager.repository;

import com.blabla.tripmanager.domain.Log;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Log entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LogRepository extends JpaRepository<Log, Long> {

    @Query("select log from Log log where log.user.login = ?#{principal.username}")
    List<Log> findByUserIsCurrentUser();

}
