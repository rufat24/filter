package com.blabla.tripmanager.repository;

import com.blabla.tripmanager.domain.SystemSettings;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SystemSettings entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SystemSettingsRepository extends JpaRepository<SystemSettings, Long> {

}
