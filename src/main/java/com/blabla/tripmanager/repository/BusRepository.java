package com.blabla.tripmanager.repository;

import com.blabla.tripmanager.domain.Bus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Bus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusRepository extends JpaRepository<Bus, Long> {
    @Query("select distinct bus from Bus bus left join fetch bus.drivers")
    List<Bus> findAllWithEagerRelationships();

    @Query("select bus from Bus bus left join fetch bus.drivers where bus.id =:id")
    Bus findOneWithEagerRelationships(@Param("id") Long id);

}
