package com.blabla.tripmanager.repository;

import com.blabla.tripmanager.domain.Tour;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;
import java.time.ZonedDateTime;
import com.blabla.tripmanager.domain.Company;
import com.blabla.tripmanager.domain.Driver;
import com.blabla.tripmanager.domain.Bus;

/**
 * Spring Data JPA repository for the Tour entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TourRepository extends JpaRepository<Tour, Long> {

    @Query("select tour from Tour tour where tour.owner.login = ?#{principal.username}")
    List<Tour> findByOwnerIsCurrentUser();

    @Query("select tour from Tour tour where tour.groupNumber=:id or :id is null " +
        "and exists(select t from Tour t where :c is null or t.company=:c and t=tour) " +
        "and exists(select t from Tour t where :b is null or t.bus=:b and t=tour) " +
        "and exists(select t from Tour t where :d is null or t.driver=:d and t=tour)" +
        "and exists(select t from Tour t where :splace is null or t.startPlace=:splace and t=tour)" +
        "and exists(select t from Tour t where :eplace is null or t.endPlace=:eplace and t=tour)" +
        "and exists(select t from Tour t where :sdate is null or :edate is null and t.startDateTime>=:sdate and t.startDateTime<=:edate and t=tour)" +
        "and exists(select t from Tour t where :ii is null or t.isInvoiced=:ii and t=tour)" +
        "and exists(select t from Tour t where :ip is null or t.isInvoicePaid=:ip and t=tour)" +
        "and exists(select doc from Documents doc where :p is null or doc.tour=tour and :p=true or doc.tour!=tour and :p=false)")
    List<Tour> findAllFilter(@Param("c") Company c, @Param("b") Bus b, @Param("d") Driver d,@Param("id")Long id, @Param("splace")String splace, @Param("eplace")String eplace, @Param("sdate")ZonedDateTime sdate, @Param("edate")ZonedDateTime edate, @Param("ii")Boolean ii,@Param("ip")Boolean ip,@Param("p")Boolean p);

}
