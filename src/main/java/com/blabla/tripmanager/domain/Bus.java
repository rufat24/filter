package com.blabla.tripmanager.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Bus.
 */
@Entity
@Table(name = "bus")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Bus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "bus_model")
    private String busModel;

    @Column(name = "registration_number")
    private String registrationNumber;

    @Column(name = "year_of_production")
    private Integer yearOfProduction;

    @Column(name = "tachograph_date")
    private ZonedDateTime tachographDate;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "bus_driver",
               joinColumns = @JoinColumn(name="buses_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="drivers_id", referencedColumnName="id"))
    private Set<Driver> drivers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusModel() {
        return busModel;
    }

    public Bus busModel(String busModel) {
        this.busModel = busModel;
        return this;
    }

    public void setBusModel(String busModel) {
        this.busModel = busModel;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public Bus registrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
        return this;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Integer getYearOfProduction() {
        return yearOfProduction;
    }

    public Bus yearOfProduction(Integer yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
        return this;
    }

    public void setYearOfProduction(Integer yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public ZonedDateTime getTachographDate() {
        return tachographDate;
    }

    public Bus tachographDate(ZonedDateTime tachographDate) {
        this.tachographDate = tachographDate;
        return this;
    }

    public void setTachographDate(ZonedDateTime tachographDate) {
        this.tachographDate = tachographDate;
    }

    public Set<Driver> getDrivers() {
        return drivers;
    }

    public Bus drivers(Set<Driver> drivers) {
        this.drivers = drivers;
        return this;
    }

    public Bus addDriver(Driver driver) {
        this.drivers.add(driver);
        driver.getBuses().add(this);
        return this;
    }

    public Bus removeDriver(Driver driver) {
        this.drivers.remove(driver);
        driver.getBuses().remove(this);
        return this;
    }

    public void setDrivers(Set<Driver> drivers) {
        this.drivers = drivers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bus bus = (Bus) o;
        if (bus.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bus.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bus{" +
            "id=" + getId() +
            ", busModel='" + getBusModel() + "'" +
            ", registrationNumber='" + getRegistrationNumber() + "'" +
            ", yearOfProduction=" + getYearOfProduction() +
            ", tachographDate='" + getTachographDate() + "'" +
            "}";
    }
}
