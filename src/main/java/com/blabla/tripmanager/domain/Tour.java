package com.blabla.tripmanager.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Tour.
 */
@Entity
@Table(name = "tour")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tour implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "group_number")
    private Integer groupNumber;

    @Column(name = "start_place")
    private String startPlace;

    @Column(name = "start_date_time")
    private ZonedDateTime startDateTime;

    @Column(name = "end_place")
    private String endPlace;

    @Column(name = "end_date_time")
    private ZonedDateTime endDateTime;

    @Column(name = "passenger_amount")
    private Integer passengerAmount;

    @Column(name = "is_invoiced")
    private Boolean isInvoiced;

    @Column(name = "payment_amount")
    private Integer paymentAmount;

    @Column(name = "is_invoice_paid")
    private Boolean isInvoicePaid;

    @Column(name = "payment_date")
    private ZonedDateTime paymentDate;

    @ManyToOne
    private Company company;

    @ManyToOne
    private Bus bus;

    @ManyToOne
    private Driver driver;

    @ManyToOne
    private User owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public Tour groupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
        return this;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getStartPlace() {
        return startPlace;
    }

    public Tour startPlace(String startPlace) {
        this.startPlace = startPlace;
        return this;
    }

    public void setStartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public ZonedDateTime getStartDateTime() {
        return startDateTime;
    }

    public Tour startDateTime(ZonedDateTime startDateTime) {
        this.startDateTime = startDateTime;
        return this;
    }

    public void setStartDateTime(ZonedDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndPlace() {
        return endPlace;
    }

    public Tour endPlace(String endPlace) {
        this.endPlace = endPlace;
        return this;
    }

    public void setEndPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public ZonedDateTime getEndDateTime() {
        return endDateTime;
    }

    public Tour endDateTime(ZonedDateTime endDateTime) {
        this.endDateTime = endDateTime;
        return this;
    }

    public void setEndDateTime(ZonedDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Integer getPassengerAmount() {
        return passengerAmount;
    }

    public Tour passengerAmount(Integer passengerAmount) {
        this.passengerAmount = passengerAmount;
        return this;
    }

    public void setPassengerAmount(Integer passengerAmount) {
        this.passengerAmount = passengerAmount;
    }

    public Boolean isIsInvoiced() {
        return isInvoiced;
    }

    public Tour isInvoiced(Boolean isInvoiced) {
        this.isInvoiced = isInvoiced;
        return this;
    }

    public void setIsInvoiced(Boolean isInvoiced) {
        this.isInvoiced = isInvoiced;
    }

    public Integer getPaymentAmount() {
        return paymentAmount;
    }

    public Tour paymentAmount(Integer paymentAmount) {
        this.paymentAmount = paymentAmount;
        return this;
    }

    public void setPaymentAmount(Integer paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Boolean isIsInvoicePaid() {
        return isInvoicePaid;
    }

    public Tour isInvoicePaid(Boolean isInvoicePaid) {
        this.isInvoicePaid = isInvoicePaid;
        return this;
    }

    public void setIsInvoicePaid(Boolean isInvoicePaid) {
        this.isInvoicePaid = isInvoicePaid;
    }

    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    public Tour paymentDate(ZonedDateTime paymentDate) {
        this.paymentDate = paymentDate;
        return this;
    }

    public void setPaymentDate(ZonedDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Company getCompany() {
        return company;
    }

    public Tour company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Bus getBus() {
        return bus;
    }

    public Tour bus(Bus bus) {
        this.bus = bus;
        return this;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Driver getDriver() {
        return driver;
    }

    public Tour driver(Driver driver) {
        this.driver = driver;
        return this;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public User getOwner() {
        return owner;
    }

    public Tour owner(User user) {
        this.owner = user;
        return this;
    }

    public void setOwner(User user) {
        this.owner = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tour tour = (Tour) o;
        if (tour.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tour.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tour{" +
            "id=" + getId() +
            ", groupNumber=" + getGroupNumber() +
            ", startPlace='" + getStartPlace() + "'" +
            ", startDateTime='" + getStartDateTime() + "'" +
            ", endPlace='" + getEndPlace() + "'" +
            ", endDateTime='" + getEndDateTime() + "'" +
            ", passengerAmount=" + getPassengerAmount() +
            ", isInvoiced='" + isIsInvoiced() + "'" +
            ", paymentAmount=" + getPaymentAmount() +
            ", isInvoicePaid='" + isIsInvoicePaid() + "'" +
            ", paymentDate='" + getPaymentDate() + "'" +
            "}";
    }
}
