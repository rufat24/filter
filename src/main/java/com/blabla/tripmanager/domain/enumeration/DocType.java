package com.blabla.tripmanager.domain.enumeration;

/**
 * The DocType enumeration.
 */
public enum DocType {
    INVOICE, PICTURE, OTHER
}
