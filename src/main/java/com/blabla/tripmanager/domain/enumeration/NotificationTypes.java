package com.blabla.tripmanager.domain.enumeration;

/**
 * The NotificationTypes enumeration.
 */
public enum NotificationTypes {
    TRANSPORTCHANGE, NOBUSDATA, TACHOMETER
}
