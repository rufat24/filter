package com.blabla.tripmanager.domain.enumeration;

/**
 * The LogEnum enumeration.
 */
public enum LogEnum {
    CHANGEBUS, CHANGEDRIVER
}
