package com.blabla.tripmanager.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SystemSettings.
 */
@Entity
@Table(name = "system_settings")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SystemSettings implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "days_to_notify")
    private Integer daysToNotify;

    @Column(name = "days_to_pick_bus")
    private Integer daysToPickBus;

    @Column(name = "max_kms_group")
    private Integer maxKmsGroup;

    @Column(name = "days_system_notify")
    private Integer daysSystemNotify;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDaysToNotify() {
        return daysToNotify;
    }

    public SystemSettings daysToNotify(Integer daysToNotify) {
        this.daysToNotify = daysToNotify;
        return this;
    }

    public void setDaysToNotify(Integer daysToNotify) {
        this.daysToNotify = daysToNotify;
    }

    public Integer getDaysToPickBus() {
        return daysToPickBus;
    }

    public SystemSettings daysToPickBus(Integer daysToPickBus) {
        this.daysToPickBus = daysToPickBus;
        return this;
    }

    public void setDaysToPickBus(Integer daysToPickBus) {
        this.daysToPickBus = daysToPickBus;
    }

    public Integer getMaxKmsGroup() {
        return maxKmsGroup;
    }

    public SystemSettings maxKmsGroup(Integer maxKmsGroup) {
        this.maxKmsGroup = maxKmsGroup;
        return this;
    }

    public void setMaxKmsGroup(Integer maxKmsGroup) {
        this.maxKmsGroup = maxKmsGroup;
    }

    public Integer getDaysSystemNotify() {
        return daysSystemNotify;
    }

    public SystemSettings daysSystemNotify(Integer daysSystemNotify) {
        this.daysSystemNotify = daysSystemNotify;
        return this;
    }

    public void setDaysSystemNotify(Integer daysSystemNotify) {
        this.daysSystemNotify = daysSystemNotify;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SystemSettings systemSettings = (SystemSettings) o;
        if (systemSettings.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), systemSettings.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SystemSettings{" +
            "id=" + getId() +
            ", daysToNotify=" + getDaysToNotify() +
            ", daysToPickBus=" + getDaysToPickBus() +
            ", maxKmsGroup=" + getMaxKmsGroup() +
            ", daysSystemNotify=" + getDaysSystemNotify() +
            "}";
    }
}
