package com.blabla.tripmanager.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import com.blabla.tripmanager.domain.enumeration.DocType;

/**
 * A Documents.
 */
@Entity
@Table(name = "documents")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Documents implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "document_type")
    private DocType documentType;

    @Column(name = "document_name")
    private String documentName;

    @Column(name = "file_hash")
    private String fileHash;

    @ManyToOne
    private Tour tour;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DocType getDocumentType() {
        return documentType;
    }

    public Documents documentType(DocType documentType) {
        this.documentType = documentType;
        return this;
    }

    public void setDocumentType(DocType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentName() {
        return documentName;
    }

    public Documents documentName(String documentName) {
        this.documentName = documentName;
        return this;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getFileHash() {
        return fileHash;
    }

    public Documents fileHash(String fileHash) {
        this.fileHash = fileHash;
        return this;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public Tour getTour() {
        return tour;
    }

    public Documents tour(Tour tour) {
        this.tour = tour;
        return this;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Documents documents = (Documents) o;
        if (documents.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), documents.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Documents{" +
            "id=" + getId() +
            ", documentType='" + getDocumentType() + "'" +
            ", documentName='" + getDocumentName() + "'" +
            ", fileHash='" + getFileHash() + "'" +
            "}";
    }
}
