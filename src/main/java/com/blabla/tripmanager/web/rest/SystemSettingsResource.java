package com.blabla.tripmanager.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.blabla.tripmanager.service.SystemSettingsService;
import com.blabla.tripmanager.web.rest.errors.BadRequestAlertException;
import com.blabla.tripmanager.web.rest.util.HeaderUtil;
import com.blabla.tripmanager.web.rest.util.PaginationUtil;
import com.blabla.tripmanager.service.dto.SystemSettingsDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SystemSettings.
 */
@RestController
@RequestMapping("/api")
public class SystemSettingsResource {

    private final Logger log = LoggerFactory.getLogger(SystemSettingsResource.class);

    private static final String ENTITY_NAME = "systemSettings";

    private final SystemSettingsService systemSettingsService;

    public SystemSettingsResource(SystemSettingsService systemSettingsService) {
        this.systemSettingsService = systemSettingsService;
    }

    /**
     * POST  /system-settings : Create a new systemSettings.
     *
     * @param systemSettingsDTO the systemSettingsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new systemSettingsDTO, or with status 400 (Bad Request) if the systemSettings has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/system-settings")
    @Timed
    public ResponseEntity<SystemSettingsDTO> createSystemSettings(@RequestBody SystemSettingsDTO systemSettingsDTO) throws URISyntaxException {
        log.debug("REST request to save SystemSettings : {}", systemSettingsDTO);
        if (systemSettingsDTO.getId() != null) {
            throw new BadRequestAlertException("A new systemSettings cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SystemSettingsDTO result = systemSettingsService.save(systemSettingsDTO);
        return ResponseEntity.created(new URI("/api/system-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /system-settings : Updates an existing systemSettings.
     *
     * @param systemSettingsDTO the systemSettingsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated systemSettingsDTO,
     * or with status 400 (Bad Request) if the systemSettingsDTO is not valid,
     * or with status 500 (Internal Server Error) if the systemSettingsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/system-settings")
    @Timed
    public ResponseEntity<SystemSettingsDTO> updateSystemSettings(@RequestBody SystemSettingsDTO systemSettingsDTO) throws URISyntaxException {
        log.debug("REST request to update SystemSettings : {}", systemSettingsDTO);
        if (systemSettingsDTO.getId() == null) {
            return createSystemSettings(systemSettingsDTO);
        }
        SystemSettingsDTO result = systemSettingsService.save(systemSettingsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, systemSettingsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /system-settings : get all the systemSettings.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of systemSettings in body
     */
    @GetMapping("/system-settings")
    @Timed
    public ResponseEntity<List<SystemSettingsDTO>> getAllSystemSettings(Pageable pageable) {
        log.debug("REST request to get a page of SystemSettings");
        Page<SystemSettingsDTO> page = systemSettingsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/system-settings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /system-settings/:id : get the "id" systemSettings.
     *
     * @param id the id of the systemSettingsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the systemSettingsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/system-settings/{id}")
    @Timed
    public ResponseEntity<SystemSettingsDTO> getSystemSettings(@PathVariable Long id) {
        log.debug("REST request to get SystemSettings : {}", id);
        SystemSettingsDTO systemSettingsDTO = systemSettingsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(systemSettingsDTO));
    }

    /**
     * DELETE  /system-settings/:id : delete the "id" systemSettings.
     *
     * @param id the id of the systemSettingsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/system-settings/{id}")
    @Timed
    public ResponseEntity<Void> deleteSystemSettings(@PathVariable Long id) {
        log.debug("REST request to delete SystemSettings : {}", id);
        systemSettingsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
