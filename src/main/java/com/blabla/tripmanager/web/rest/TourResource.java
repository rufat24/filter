package com.blabla.tripmanager.web.rest;

import com.blabla.tripmanager.service.UserService;
import com.codahale.metrics.annotation.Timed;
import com.blabla.tripmanager.service.TourService;
import com.blabla.tripmanager.web.rest.errors.BadRequestAlertException;
import com.blabla.tripmanager.web.rest.util.HeaderUtil;
import com.blabla.tripmanager.web.rest.util.PaginationUtil;
import com.blabla.tripmanager.service.dto.TourDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.blabla.tripmanager.domain.Company;
import com.blabla.tripmanager.domain.Bus;
import com.blabla.tripmanager.domain.Driver;
import com.blabla.tripmanager.security.SecurityUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tour.
 */
@RestController
@RequestMapping("/api")
public class TourResource {

    private final Logger log = LoggerFactory.getLogger(TourResource.class);

    private static final String ENTITY_NAME = "tour";

    private final TourService tourService;

    public TourResource(TourService tourService) {

        this.tourService = tourService;
    }

    /**
     * POST  /tours : Create a new tour.
     *
     * @param tourDTO the tourDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tourDTO, or with status 400 (Bad Request) if the tour has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tours")
    @Timed
    public ResponseEntity<TourDTO> createTour(@RequestBody TourDTO tourDTO) throws URISyntaxException {
        log.debug("REST request to save Tour : {}", tourDTO);
        if (tourDTO.getId() != null) {
            throw new BadRequestAlertException("A new tour cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TourDTO result = tourService.save(tourDTO);
        return ResponseEntity.created(new URI("/api/tours/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/tours/filter/")
    @Timed
    public List<TourDTO>getAllToursFilter(@RequestParam(value="company", required = false) Company c, @RequestParam(value="bus", required = false) Bus b, @RequestParam(value="driver", required = false) Driver d, @RequestParam(value="groupnumber", required = false) Long id,@RequestParam(value="splace", required = false) String splace, @RequestParam(value="eplace", required = false) String eplace,@RequestParam(value="sdate", required = false) ZonedDateTime sdate,@RequestParam(value="edate", required = false) ZonedDateTime edate, @RequestParam(value="isinv", required = false) Boolean ii, @RequestParam(value="ispaid", required = false) Boolean ip, @RequestParam(value="photo", required = false) Boolean p ) {
        return tourService.findAllFilter(c,b,d,id,splace,eplace,sdate,edate,ii,ip,p);
    }
    /**
     * PUT  /tours : Updates an existing tour.
     *
     * @param tourDTO the tourDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tourDTO,
     * or with status 400 (Bad Request) if the tourDTO is not valid,
     * or with status 500 (Internal Server Error) if the tourDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tours")
    @Timed
    public ResponseEntity<TourDTO> updateTour(@RequestBody TourDTO tourDTO) throws URISyntaxException {
        log.debug("REST request to update Tour : {}", tourDTO);
        if (tourDTO.getId() == null) {
            return createTour(tourDTO);
        }

        TourDTO result = tourService.save(tourDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tourDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tours : get all the tours.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tours in body
     */
    @GetMapping("/tours")
    @Timed
    public ResponseEntity<List<TourDTO>> getAllTours(Pageable pageable) {
        log.debug("REST request to get a page of Tours");
        Page<TourDTO> page = tourService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tours");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tours/:id : get the "id" tour.
     *
     * @param id the id of the tourDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tourDTO, or with status 404 (Not Found)
     */
    @GetMapping("/tours/{id}")
    @Timed
    public ResponseEntity<TourDTO> getTour(@PathVariable Long id) {
        log.debug("REST request to get Tour : {}", id);
        TourDTO tourDTO = tourService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tourDTO));
    }

    /**
     * DELETE  /tours/:id : delete the "id" tour.
     *
     * @param id the id of the tourDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tours/{id}")
    @Timed
    public ResponseEntity<Void> deleteTour(@PathVariable Long id) {
        log.debug("REST request to delete Tour : {}", id);
        tourService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
