/**
 * View Models used by Spring MVC REST controllers.
 */
package com.blabla.tripmanager.web.rest.vm;
