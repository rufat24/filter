package com.blabla.tripmanager.web.rest;

import com.blabla.tripmanager.service.DocumentsService;
import com.blabla.tripmanager.service.dto.DocumentsDTO;
import com.blabla.tripmanager.web.rest.errors.BadRequestAlertException;
import com.blabla.tripmanager.web.rest.util.HeaderUtil;
import com.blabla.tripmanager.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import com.blabla.tripmanager.util.FileManagerUtil;
import liquibase.util.file.FilenameUtils;
import org.springframework.http.MediaType;
import com.blabla.tripmanager.security.SecurityUtils;



/**
 * REST controller for managing Documents.
 */
@RestController
@RequestMapping("/api")
public class DocumentsResource {

    private final Logger log = LoggerFactory.getLogger(DocumentsResource.class);

    private static final String ENTITY_NAME = "documents";

    private final DocumentsService documentsService;

    public DocumentsResource(DocumentsService documentsService) {
        this.documentsService = documentsService;
    }

    /**
     * POST  /documents : Create a new documents.
     *
     * @param documentsDTO the documentsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new documentsDTO, or with status 400 (Bad Request) if the documents has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/documents")
    @Timed
    public ResponseEntity<DocumentsDTO> createDocuments(@RequestBody DocumentsDTO documentsDTO) throws URISyntaxException {
        log.debug("REST request to save Documents : {}", documentsDTO);
        if (documentsDTO.getId() != null) {
            throw new BadRequestAlertException("A new documents cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DocumentsDTO result = documentsService.save(documentsDTO);
        return ResponseEntity.created(new URI("/api/documents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    @PostMapping("/documents/upload/{id}")
    public ResponseEntity<DocumentsDTO> uploadImage(@PathVariable Long id, @RequestPart MultipartFile document) {
        if(document!=null){
            DocumentsDTO result = documentsService.saveFile(id, document);
            return  ResponseEntity.ok()
                .body(result);
        }else{
            return null;
        }
    }

    @GetMapping("/documents/download/{hash:.+}")
    @Timed
    public ResponseEntity<byte[]> getDocumentsById(@PathVariable String hash) {
        if(SecurityUtils.isCurrentUserInRole("ROLE_INVOICE_ED") || SecurityUtils.isCurrentUserInRole("ROLE_INVOICE_SUBED")) {
            ResponseEntity.notFound();
        }
        byte[] image = FileManagerUtil.downloadImage(hash);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("image/"+ FilenameUtils.getExtension(hash))).body(image);
    }

    /**
     * PUT  /documents : Updates an existing documents.
     *
     * @param documentsDTO the documentsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated documentsDTO,
     * or with status 400 (Bad Request) if the documentsDTO is not valid,
     * or with status 500 (Internal Server Error) if the documentsDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/documents")
    @Timed
    public ResponseEntity<DocumentsDTO> updateDocuments(@RequestBody DocumentsDTO documentsDTO) throws URISyntaxException {
        log.debug("REST request to update Documents : {}", documentsDTO);
        if (documentsDTO.getId() == null) {
            return createDocuments(documentsDTO);
        }
        DocumentsDTO result = documentsService.save(documentsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, documentsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /documents : get all the documents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of documents in body
     */
    @GetMapping("/documents")
    @Timed
    public ResponseEntity<List<DocumentsDTO>> getAllDocuments(Pageable pageable) {
        log.debug("REST request to get a page of Documents");
        Page<DocumentsDTO> page = documentsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /documents/:id : get the "id" documents.
     *
     * @param id the id of the documentsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the documentsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/documents/{id}")
    @Timed
    public ResponseEntity<DocumentsDTO> getDocuments(@PathVariable Long id) {
        log.debug("REST request to get Documents : {}", id);
        DocumentsDTO documentsDTO = documentsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(documentsDTO));
    }

    /**
     * DELETE  /documents/:id : delete the "id" documents.
     *
     * @param id the id of the documentsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/documents/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocuments(@PathVariable Long id) {
        log.debug("REST request to delete Documents : {}", id);
        documentsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
