import { BaseEntity } from './../../shared';

export class Driver implements BaseEntity {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public telephone?: string,
        public address?: string,
        public email?: string,
        public pesel?: string,
        public daysWorked?: number,
        public image?: string,
        public buses?: BaseEntity[],
    ) {
    }
}
