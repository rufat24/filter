import { BaseEntity } from './../../shared';

export const enum LogEnum {
    'CHANGEBUS',
    'CHANGEDRIVER'
}

export class Log implements BaseEntity {
    constructor(
        public id?: number,
        public actionType?: LogEnum,
        public description?: string,
        public userId?: number,
    ) {
    }
}
