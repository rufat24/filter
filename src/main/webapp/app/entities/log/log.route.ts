import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { LogComponent } from './log.component';
import { LogDetailComponent } from './log-detail.component';
import { LogPopupComponent } from './log-dialog.component';
import { LogDeletePopupComponent } from './log-delete-dialog.component';

@Injectable()
export class LogResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const logRoute: Routes = [
    {
        path: 'log',
        component: LogComponent,
        resolve: {
            'pagingParams': LogResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Logs'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'log/:id',
        component: LogDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Logs'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const logPopupRoute: Routes = [
    {
        path: 'log-new',
        component: LogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Logs'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'log/:id/edit',
        component: LogPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Logs'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'log/:id/delete',
        component: LogDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Logs'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
