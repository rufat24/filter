import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import { TestAdminModule } from '../../admin/admin.module';
import {
    LogService,
    LogPopupService,
    LogComponent,
    LogDetailComponent,
    LogDialogComponent,
    LogPopupComponent,
    LogDeletePopupComponent,
    LogDeleteDialogComponent,
    logRoute,
    logPopupRoute,
    LogResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...logRoute,
    ...logPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        TestAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LogComponent,
        LogDetailComponent,
        LogDialogComponent,
        LogDeleteDialogComponent,
        LogPopupComponent,
        LogDeletePopupComponent,
    ],
    entryComponents: [
        LogComponent,
        LogDialogComponent,
        LogPopupComponent,
        LogDeleteDialogComponent,
        LogDeletePopupComponent,
    ],
    providers: [
        LogService,
        LogPopupService,
        LogResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestLogModule {}
