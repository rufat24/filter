import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Log } from './log.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Log>;

@Injectable()
export class LogService {

    private resourceUrl =  SERVER_API_URL + 'api/logs';

    constructor(private http: HttpClient) { }

    create(log: Log): Observable<EntityResponseType> {
        const copy = this.convert(log);
        return this.http.post<Log>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(log: Log): Observable<EntityResponseType> {
        const copy = this.convert(log);
        return this.http.put<Log>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Log>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Log[]>> {
        const options = createRequestOption(req);
        return this.http.get<Log[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Log[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Log = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Log[]>): HttpResponse<Log[]> {
        const jsonResponse: Log[] = res.body;
        const body: Log[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Log.
     */
    private convertItemFromServer(log: Log): Log {
        const copy: Log = Object.assign({}, log);
        return copy;
    }

    /**
     * Convert a Log to a JSON which can be sent to the server.
     */
    private convert(log: Log): Log {
        const copy: Log = Object.assign({}, log);
        return copy;
    }
}
