import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Tour } from './tour.model';
import { TourPopupService } from './tour-popup.service';
import { TourService } from './tour.service';
import { Company, CompanyService } from '../company';
import { Bus, BusService } from '../bus';
import { Driver, DriverService } from '../driver';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-tour-dialog',
    templateUrl: './tour-dialog.component.html'
})
export class TourDialogComponent implements OnInit {

    tour: Tour;
    isSaving: boolean;

    companies: Company[];

    buses: Bus[];

    drivers: Driver[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tourService: TourService,
        private companyService: CompanyService,
        private busService: BusService,
        private driverService: DriverService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.companyService.query()
            .subscribe((res: HttpResponse<Company[]>) => { this.companies = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.busService.query()
            .subscribe((res: HttpResponse<Bus[]>) => { this.buses = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.driverService.query()
            .subscribe((res: HttpResponse<Driver[]>) => { this.drivers = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tour.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tourService.update(this.tour));
        } else {
            this.subscribeToSaveResponse(
                this.tourService.create(this.tour));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Tour>>) {
        result.subscribe((res: HttpResponse<Tour>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Tour) {
        this.eventManager.broadcast({ name: 'tourListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCompanyById(index: number, item: Company) {
        return item.id;
    }

    trackBusById(index: number, item: Bus) {
        return item.id;
    }

    trackDriverById(index: number, item: Driver) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-tour-popup',
    template: ''
})
export class TourPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tourPopupService: TourPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tourPopupService
                    .open(TourDialogComponent as Component, params['id']);
            } else {
                this.tourPopupService
                    .open(TourDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
