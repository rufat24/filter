import { BaseEntity } from './../../shared';

export class Tour implements BaseEntity {
    constructor(
        public id?: number,
        public groupNumber?: number,
        public startPlace?: string,
        public startDateTime?: any,
        public endPlace?: string,
        public endDateTime?: any,
        public passengerAmount?: number,
        public isInvoiced?: boolean,
        public paymentAmount?: number,
        public isInvoicePaid?: boolean,
        public paymentDate?: any,
        public companyId?: number,
        public busId?: number,
        public driverId?: number,
        public ownerId?: number,
    ) {
        this.isInvoiced = false;
        this.isInvoicePaid = false;
    }
}
