import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import { TestAdminModule } from '../../admin/admin.module';
import {
    TourService,
    TourPopupService,
    TourComponent,
    TourDetailComponent,
    TourDialogComponent,
    TourPopupComponent,
    TourDeletePopupComponent,
    TourDeleteDialogComponent,
    tourRoute,
    tourPopupRoute,
    TourResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...tourRoute,
    ...tourPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        TestAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TourComponent,
        TourDetailComponent,
        TourDialogComponent,
        TourDeleteDialogComponent,
        TourPopupComponent,
        TourDeletePopupComponent,
    ],
    entryComponents: [
        TourComponent,
        TourDialogComponent,
        TourPopupComponent,
        TourDeleteDialogComponent,
        TourDeletePopupComponent,
    ],
    providers: [
        TourService,
        TourPopupService,
        TourResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestTourModule {}
