import { BaseEntity } from './../../shared';

export const enum NotificationTypes {
    'TRANSPORTCHANGE',
    'NOBUSDATA',
    'TACHOMETER'
}

export class Notification implements BaseEntity {
    constructor(
        public id?: number,
        public notificationText?: string,
        public actionType?: NotificationTypes,
        public userId?: number,
        public tourId?: number,
    ) {
    }
}
