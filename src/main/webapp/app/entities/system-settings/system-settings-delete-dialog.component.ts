import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SystemSettings } from './system-settings.model';
import { SystemSettingsPopupService } from './system-settings-popup.service';
import { SystemSettingsService } from './system-settings.service';

@Component({
    selector: 'jhi-system-settings-delete-dialog',
    templateUrl: './system-settings-delete-dialog.component.html'
})
export class SystemSettingsDeleteDialogComponent {

    systemSettings: SystemSettings;

    constructor(
        private systemSettingsService: SystemSettingsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.systemSettingsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'systemSettingsListModification',
                content: 'Deleted an systemSettings'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-system-settings-delete-popup',
    template: ''
})
export class SystemSettingsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private systemSettingsPopupService: SystemSettingsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.systemSettingsPopupService
                .open(SystemSettingsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
