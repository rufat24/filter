import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SystemSettings } from './system-settings.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SystemSettings>;

@Injectable()
export class SystemSettingsService {

    private resourceUrl =  SERVER_API_URL + 'api/system-settings';

    constructor(private http: HttpClient) { }

    create(systemSettings: SystemSettings): Observable<EntityResponseType> {
        const copy = this.convert(systemSettings);
        return this.http.post<SystemSettings>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(systemSettings: SystemSettings): Observable<EntityResponseType> {
        const copy = this.convert(systemSettings);
        return this.http.put<SystemSettings>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SystemSettings>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SystemSettings[]>> {
        const options = createRequestOption(req);
        return this.http.get<SystemSettings[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SystemSettings[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SystemSettings = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SystemSettings[]>): HttpResponse<SystemSettings[]> {
        const jsonResponse: SystemSettings[] = res.body;
        const body: SystemSettings[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SystemSettings.
     */
    private convertItemFromServer(systemSettings: SystemSettings): SystemSettings {
        const copy: SystemSettings = Object.assign({}, systemSettings);
        return copy;
    }

    /**
     * Convert a SystemSettings to a JSON which can be sent to the server.
     */
    private convert(systemSettings: SystemSettings): SystemSettings {
        const copy: SystemSettings = Object.assign({}, systemSettings);
        return copy;
    }
}
