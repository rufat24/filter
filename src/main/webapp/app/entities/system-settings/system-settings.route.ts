import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SystemSettingsComponent } from './system-settings.component';
import { SystemSettingsDetailComponent } from './system-settings-detail.component';
import { SystemSettingsPopupComponent } from './system-settings-dialog.component';
import { SystemSettingsDeletePopupComponent } from './system-settings-delete-dialog.component';

@Injectable()
export class SystemSettingsResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const systemSettingsRoute: Routes = [
    {
        path: 'system-settings',
        component: SystemSettingsComponent,
        resolve: {
            'pagingParams': SystemSettingsResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SystemSettings'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'system-settings/:id',
        component: SystemSettingsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SystemSettings'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const systemSettingsPopupRoute: Routes = [
    {
        path: 'system-settings-new',
        component: SystemSettingsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SystemSettings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'system-settings/:id/edit',
        component: SystemSettingsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SystemSettings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'system-settings/:id/delete',
        component: SystemSettingsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SystemSettings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
