import { BaseEntity } from './../../shared';

export class SystemSettings implements BaseEntity {
    constructor(
        public id?: number,
        public daysToNotify?: number,
        public daysToPickBus?: number,
        public maxKmsGroup?: number,
        public daysSystemNotify?: number,
    ) {
    }
}
