import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    SystemSettingsService,
    SystemSettingsPopupService,
    SystemSettingsComponent,
    SystemSettingsDetailComponent,
    SystemSettingsDialogComponent,
    SystemSettingsPopupComponent,
    SystemSettingsDeletePopupComponent,
    SystemSettingsDeleteDialogComponent,
    systemSettingsRoute,
    systemSettingsPopupRoute,
    SystemSettingsResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...systemSettingsRoute,
    ...systemSettingsPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SystemSettingsComponent,
        SystemSettingsDetailComponent,
        SystemSettingsDialogComponent,
        SystemSettingsDeleteDialogComponent,
        SystemSettingsPopupComponent,
        SystemSettingsDeletePopupComponent,
    ],
    entryComponents: [
        SystemSettingsComponent,
        SystemSettingsDialogComponent,
        SystemSettingsPopupComponent,
        SystemSettingsDeleteDialogComponent,
        SystemSettingsDeletePopupComponent,
    ],
    providers: [
        SystemSettingsService,
        SystemSettingsPopupService,
        SystemSettingsResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestSystemSettingsModule {}
