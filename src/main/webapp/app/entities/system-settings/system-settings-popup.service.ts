import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SystemSettings } from './system-settings.model';
import { SystemSettingsService } from './system-settings.service';

@Injectable()
export class SystemSettingsPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private systemSettingsService: SystemSettingsService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.systemSettingsService.find(id)
                    .subscribe((systemSettingsResponse: HttpResponse<SystemSettings>) => {
                        const systemSettings: SystemSettings = systemSettingsResponse.body;
                        this.ngbModalRef = this.systemSettingsModalRef(component, systemSettings);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.systemSettingsModalRef(component, new SystemSettings());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    systemSettingsModalRef(component: Component, systemSettings: SystemSettings): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.systemSettings = systemSettings;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
