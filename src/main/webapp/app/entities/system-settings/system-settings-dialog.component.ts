import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SystemSettings } from './system-settings.model';
import { SystemSettingsPopupService } from './system-settings-popup.service';
import { SystemSettingsService } from './system-settings.service';

@Component({
    selector: 'jhi-system-settings-dialog',
    templateUrl: './system-settings-dialog.component.html'
})
export class SystemSettingsDialogComponent implements OnInit {

    systemSettings: SystemSettings;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private systemSettingsService: SystemSettingsService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.systemSettings.id !== undefined) {
            this.subscribeToSaveResponse(
                this.systemSettingsService.update(this.systemSettings));
        } else {
            this.subscribeToSaveResponse(
                this.systemSettingsService.create(this.systemSettings));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SystemSettings>>) {
        result.subscribe((res: HttpResponse<SystemSettings>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SystemSettings) {
        this.eventManager.broadcast({ name: 'systemSettingsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-system-settings-popup',
    template: ''
})
export class SystemSettingsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private systemSettingsPopupService: SystemSettingsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.systemSettingsPopupService
                    .open(SystemSettingsDialogComponent as Component, params['id']);
            } else {
                this.systemSettingsPopupService
                    .open(SystemSettingsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
