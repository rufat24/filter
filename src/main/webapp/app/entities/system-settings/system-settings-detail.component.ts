import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SystemSettings } from './system-settings.model';
import { SystemSettingsService } from './system-settings.service';

@Component({
    selector: 'jhi-system-settings-detail',
    templateUrl: './system-settings-detail.component.html'
})
export class SystemSettingsDetailComponent implements OnInit, OnDestroy {

    systemSettings: SystemSettings;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private systemSettingsService: SystemSettingsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSystemSettings();
    }

    load(id) {
        this.systemSettingsService.find(id)
            .subscribe((systemSettingsResponse: HttpResponse<SystemSettings>) => {
                this.systemSettings = systemSettingsResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSystemSettings() {
        this.eventSubscriber = this.eventManager.subscribe(
            'systemSettingsListModification',
            (response) => this.load(this.systemSettings.id)
        );
    }
}
