import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    DocumentsService,
    DocumentsPopupService,
    DocumentsComponent,
    DocumentsDetailComponent,
    DocumentsDialogComponent,
    DocumentsPopupComponent,
    DocumentsDeletePopupComponent,
    DocumentsDeleteDialogComponent,
    documentsRoute,
    documentsPopupRoute,
    DocumentsResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...documentsRoute,
    ...documentsPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DocumentsComponent,
        DocumentsDetailComponent,
        DocumentsDialogComponent,
        DocumentsDeleteDialogComponent,
        DocumentsPopupComponent,
        DocumentsDeletePopupComponent,
    ],
    entryComponents: [
        DocumentsComponent,
        DocumentsDialogComponent,
        DocumentsPopupComponent,
        DocumentsDeleteDialogComponent,
        DocumentsDeletePopupComponent,
    ],
    providers: [
        DocumentsService,
        DocumentsPopupService,
        DocumentsResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestDocumentsModule {}
