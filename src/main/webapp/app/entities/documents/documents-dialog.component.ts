import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Documents } from './documents.model';
import { DocumentsPopupService } from './documents-popup.service';
import { DocumentsService } from './documents.service';
import { Tour, TourService } from '../tour';

@Component({
    selector: 'jhi-documents-dialog',
    templateUrl: './documents-dialog.component.html'
})
export class DocumentsDialogComponent implements OnInit {

    documents: Documents;
    isSaving: boolean;

    tours: Tour[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private documentsService: DocumentsService,
        private tourService: TourService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.tourService.query()
            .subscribe((res: HttpResponse<Tour[]>) => { this.tours = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.documents.id !== undefined) {
            this.subscribeToSaveResponse(
                this.documentsService.update(this.documents));
        } else {
            this.subscribeToSaveResponse(
                this.documentsService.create(this.documents));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Documents>>) {
        result.subscribe((res: HttpResponse<Documents>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Documents) {
        this.eventManager.broadcast({ name: 'documentsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTourById(index: number, item: Tour) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-documents-popup',
    template: ''
})
export class DocumentsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private documentsPopupService: DocumentsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.documentsPopupService
                    .open(DocumentsDialogComponent as Component, params['id']);
            } else {
                this.documentsPopupService
                    .open(DocumentsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
