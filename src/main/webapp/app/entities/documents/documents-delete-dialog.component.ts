import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Documents } from './documents.model';
import { DocumentsPopupService } from './documents-popup.service';
import { DocumentsService } from './documents.service';

@Component({
    selector: 'jhi-documents-delete-dialog',
    templateUrl: './documents-delete-dialog.component.html'
})
export class DocumentsDeleteDialogComponent {

    documents: Documents;

    constructor(
        private documentsService: DocumentsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.documentsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'documentsListModification',
                content: 'Deleted an documents'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-documents-delete-popup',
    template: ''
})
export class DocumentsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private documentsPopupService: DocumentsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.documentsPopupService
                .open(DocumentsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
