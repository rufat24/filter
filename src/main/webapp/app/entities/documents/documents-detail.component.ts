import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Documents } from './documents.model';
import { DocumentsService } from './documents.service';

@Component({
    selector: 'jhi-documents-detail',
    templateUrl: './documents-detail.component.html'
})
export class DocumentsDetailComponent implements OnInit, OnDestroy {

    documents: Documents;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private documentsService: DocumentsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDocuments();
    }

    load(id) {
        this.documentsService.find(id)
            .subscribe((documentsResponse: HttpResponse<Documents>) => {
                this.documents = documentsResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDocuments() {
        this.eventSubscriber = this.eventManager.subscribe(
            'documentsListModification',
            (response) => this.load(this.documents.id)
        );
    }
}
