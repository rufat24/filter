import { BaseEntity } from './../../shared';

export const enum DocType {
    'INVOICE',
    'PICTURE',
    'OTHER'
}

export class Documents implements BaseEntity {
    constructor(
        public id?: number,
        public documentType?: DocType,
        public documentName?: string,
        public fileHash?: string,
        public tourId?: number,
    ) {
    }
}
