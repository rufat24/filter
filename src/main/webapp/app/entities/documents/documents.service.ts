import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Documents } from './documents.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Documents>;

@Injectable()
export class DocumentsService {

    private resourceUrl =  SERVER_API_URL + 'api/documents';

    constructor(private http: HttpClient) { }

    create(documents: Documents): Observable<EntityResponseType> {
        const copy = this.convert(documents);
        return this.http.post<Documents>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(documents: Documents): Observable<EntityResponseType> {
        const copy = this.convert(documents);
        return this.http.put<Documents>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Documents>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Documents[]>> {
        const options = createRequestOption(req);
        return this.http.get<Documents[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Documents[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Documents = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Documents[]>): HttpResponse<Documents[]> {
        const jsonResponse: Documents[] = res.body;
        const body: Documents[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Documents.
     */
    private convertItemFromServer(documents: Documents): Documents {
        const copy: Documents = Object.assign({}, documents);
        return copy;
    }

    /**
     * Convert a Documents to a JSON which can be sent to the server.
     */
    private convert(documents: Documents): Documents {
        const copy: Documents = Object.assign({}, documents);
        return copy;
    }
}
