import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Bus } from './bus.model';
import { BusPopupService } from './bus-popup.service';
import { BusService } from './bus.service';

@Component({
    selector: 'jhi-bus-delete-dialog',
    templateUrl: './bus-delete-dialog.component.html'
})
export class BusDeleteDialogComponent {

    bus: Bus;

    constructor(
        private busService: BusService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.busService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'busListModification',
                content: 'Deleted an bus'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-bus-delete-popup',
    template: ''
})
export class BusDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private busPopupService: BusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.busPopupService
                .open(BusDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
