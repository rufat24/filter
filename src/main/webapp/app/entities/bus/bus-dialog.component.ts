import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Bus } from './bus.model';
import { BusPopupService } from './bus-popup.service';
import { BusService } from './bus.service';
import { Driver, DriverService } from '../driver';

@Component({
    selector: 'jhi-bus-dialog',
    templateUrl: './bus-dialog.component.html'
})
export class BusDialogComponent implements OnInit {

    bus: Bus;
    isSaving: boolean;

    drivers: Driver[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private busService: BusService,
        private driverService: DriverService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.driverService.query()
            .subscribe((res: HttpResponse<Driver[]>) => { this.drivers = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bus.id !== undefined) {
            this.subscribeToSaveResponse(
                this.busService.update(this.bus));
        } else {
            this.subscribeToSaveResponse(
                this.busService.create(this.bus));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Bus>>) {
        result.subscribe((res: HttpResponse<Bus>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Bus) {
        this.eventManager.broadcast({ name: 'busListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackDriverById(index: number, item: Driver) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-bus-popup',
    template: ''
})
export class BusPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private busPopupService: BusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.busPopupService
                    .open(BusDialogComponent as Component, params['id']);
            } else {
                this.busPopupService
                    .open(BusDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
