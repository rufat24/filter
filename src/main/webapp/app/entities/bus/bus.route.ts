import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BusComponent } from './bus.component';
import { BusDetailComponent } from './bus-detail.component';
import { BusPopupComponent } from './bus-dialog.component';
import { BusDeletePopupComponent } from './bus-delete-dialog.component';

@Injectable()
export class BusResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const busRoute: Routes = [
    {
        path: 'bus',
        component: BusComponent,
        resolve: {
            'pagingParams': BusResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Buses'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'bus/:id',
        component: BusDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Buses'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const busPopupRoute: Routes = [
    {
        path: 'bus-new',
        component: BusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Buses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bus/:id/edit',
        component: BusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Buses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bus/:id/delete',
        component: BusDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Buses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
