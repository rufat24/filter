import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Bus } from './bus.model';
import { BusService } from './bus.service';

@Component({
    selector: 'jhi-bus-detail',
    templateUrl: './bus-detail.component.html'
})
export class BusDetailComponent implements OnInit, OnDestroy {

    bus: Bus;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private busService: BusService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBuses();
    }

    load(id) {
        this.busService.find(id)
            .subscribe((busResponse: HttpResponse<Bus>) => {
                this.bus = busResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBuses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'busListModification',
            (response) => this.load(this.bus.id)
        );
    }
}
