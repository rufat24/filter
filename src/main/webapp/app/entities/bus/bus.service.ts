import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Bus } from './bus.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Bus>;

@Injectable()
export class BusService {

    private resourceUrl =  SERVER_API_URL + 'api/buses';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(bus: Bus): Observable<EntityResponseType> {
        const copy = this.convert(bus);
        return this.http.post<Bus>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bus: Bus): Observable<EntityResponseType> {
        const copy = this.convert(bus);
        return this.http.put<Bus>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Bus>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Bus[]>> {
        const options = createRequestOption(req);
        return this.http.get<Bus[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Bus[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Bus = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Bus[]>): HttpResponse<Bus[]> {
        const jsonResponse: Bus[] = res.body;
        const body: Bus[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Bus.
     */
    private convertItemFromServer(bus: Bus): Bus {
        const copy: Bus = Object.assign({}, bus);
        copy.tachographDate = this.dateUtils
            .convertDateTimeFromServer(bus.tachographDate);
        return copy;
    }

    /**
     * Convert a Bus to a JSON which can be sent to the server.
     */
    private convert(bus: Bus): Bus {
        const copy: Bus = Object.assign({}, bus);

        copy.tachographDate = this.dateUtils.toDate(bus.tachographDate);
        return copy;
    }
}
