import { BaseEntity } from './../../shared';

export class Bus implements BaseEntity {
    constructor(
        public id?: number,
        public busModel?: string,
        public registrationNumber?: string,
        public yearOfProduction?: number,
        public tachographDate?: any,
        public drivers?: BaseEntity[],
    ) {
    }
}
