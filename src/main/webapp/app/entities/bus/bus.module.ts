import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    BusService,
    BusPopupService,
    BusComponent,
    BusDetailComponent,
    BusDialogComponent,
    BusPopupComponent,
    BusDeletePopupComponent,
    BusDeleteDialogComponent,
    busRoute,
    busPopupRoute,
    BusResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...busRoute,
    ...busPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BusComponent,
        BusDetailComponent,
        BusDialogComponent,
        BusDeleteDialogComponent,
        BusPopupComponent,
        BusDeletePopupComponent,
    ],
    entryComponents: [
        BusComponent,
        BusDialogComponent,
        BusPopupComponent,
        BusDeleteDialogComponent,
        BusDeletePopupComponent,
    ],
    providers: [
        BusService,
        BusPopupService,
        BusResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestBusModule {}
