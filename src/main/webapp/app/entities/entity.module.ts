import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TestTourModule } from './tour/tour.module';
import { TestBusModule } from './bus/bus.module';
import { TestDriverModule } from './driver/driver.module';
import { TestCompanyModule } from './company/company.module';
import { TestLogModule } from './log/log.module';
import { TestSystemSettingsModule } from './system-settings/system-settings.module';
import { TestNotificationModule } from './notification/notification.module';
import { TestDocumentsModule } from './documents/documents.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TestTourModule,
        TestBusModule,
        TestDriverModule,
        TestCompanyModule,
        TestLogModule,
        TestSystemSettingsModule,
        TestNotificationModule,
        TestDocumentsModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestEntityModule {}
